"""This module writes LAMMPS scripts.

It's imported by the write_friction module."""

import numpy as np

boundary_list = ["p", "s", "f", "m"]
atom_style_list = [
    "angle",
    "atomic",
    "body",
    "bond",
    "charge",
    "dielectric",
    "dipole",
    "dpd",
    "edpd",
    "electron",
    "ellipsoid",
    "full",
    "line",
    "mpdp",
    "mesont",
    "molecular",
    "peri",
    "smd",
    "sph",
    "sphere",
    "spin",
    "tdpd",
    "template",
    "tri",
    "wavepacket",
]
units_list = ["lj", "real", "metal", "si", "cgs", "electron", "micro", "nano"]


def write_script_horizontal(
    fname,
    units,
    boundary,
    atom_style,
    box,
    box_size,
    atom_types,
    types,
    atom_info,
    pot,
    timestep,
    moving_regions,
    disp,
    vel,
    temp_name,
    dump_name,
    n_runs,
    mlip,
    pot_name,
    dir_name
):
    region_str = ""
    file = open(fname, "w")
    file.write(
        f"# This script loads the .data files of two system to simulate the \
friction between them. In this case, it loads\n\
# a {atom_info[0, 0]}-{atom_info[1, 0]} system with {disp[2]} \
# Angstroms between them. The velocity of the system is {vel[0]}\n\
# Angstroms/fs.\n"
    )
    file.write("units {}\n".format(units))
    file.write(f"boundary {boundary[0]} {boundary[1]} {boundary[2]}\n")
    file.write(f"atom_style {atom_style}\n\n")
    file.write(
        f"read_data {dir_name}\
/data/{atom_info[0, 0]}.eq.data extra/atom/types \
{atom_info[1, 5]} group g16\n"
    )
    file.write(
        f"read_data \
{dir_name}/data/{atom_info[1, 0]}.eq.data add append offset \
{atom_info[0, 5]} {atom_info[0, 5]} {atom_info[0, 5]} {atom_info[0, 5]} \
{atom_info[0, 5]} shift {disp[0]} {disp[1]} {disp[2]} group g17\n\n"
    )
    file.write(f"displace_atoms g16 rotate {box_size[0]/2} 0 0 0 1 0 180\n\n")
    for n_region in range(len(box)):
        file.write(
            f"region r{n_region+1} block \
{box[n_region, 0, 0]} {box[n_region, 0, 1]} \
{box[n_region, 1, 0]} {box[n_region, 1, 1]} \
{box[n_region, 2, 0]} {box[n_region, 2, 1]} units box\n"
        )
        region_str += "r{} ".format(n_region + 1)
    create_groups(file, box)
    if not mlip:
        pair_style(file, pot, atom_types, types)
    else:
        file.write(f"\npair_style mlip ../potentials/{pot_name}")
        file.write(f"\npair_coeff * *\n")
    mass(file, atom_info, atom_types)
    file.write(f"\ntimestep {timestep}\n")
    file.write("\nneighbor 0.3 bin\nneigh_modify delay 5\n")
    fix_nve(file, 1, 7)
    list_print = []
    compute_energy(file, list_print)
    compute_T_partial(file, moving_regions[:-1], list_print)
    fix_print(file, temp_name, list_print)
    fix_move_linear(file, vel)
    dump(file, dump_name)
    thermo(file, 10)
    run(file, n_runs)


def write_script_eq(
    fname,
    units,
    boundary,
    atom_style,
    box,
    atom_types,
    types,
    atom_info,
    pot,
    timestep,
    moving_regions,
    n_runs,
    disp,
    mat,
    T,
    partial,
    mlip,
    pot_name,
    dir_name
):
    file = open(fname, "w")
    file.write("units {}\n".format(units))
    file.write(
        "boundary {} {} {}\n".format(boundary[0], boundary[1], boundary[2])
    )
    file.write("atom_style {}\n\n".format(atom_style))
    for n_region in range(len(box)):
        file.write(
            "region r{} block {} {} {} {} {} {} units box\n".format(
                n_region + 1,
                box[n_region, 0, 0],
                box[n_region, 0, 1],
                box[n_region, 1, 0],
                box[n_region, 1, 1],
                box[n_region, 2, 0],
                box[n_region, 2, 1],
            )
        )
    file.write("create_box {} r1\n\n".format(atom_types))
    create_atoms(file, atom_info)
    create_groups(file, box)
    if not mlip:
        pair_style(file, pot, atom_types, types)
    else:
        file.write(f"\npair_style mlip ../potentials/{pot_name}")
        file.write(f"\npair_coeff * *\n")
    mass(file, atom_info, atom_types)
    file.write(f"\ntimestep {timestep}\n")
    if mlip:
        file.write("\nneighbor 0.3 nsq\nneigh_modify delay 5\n")
    else:
        file.write("\nneighbor 0.3 bin\nneigh_modify delay 5\n")
    fix_nvt(file, 1, 1, 10, T)
    list_print = []
    compute_T(file, moving_regions, list_print)
    if partial:
        dump(
        file,
        f"{dir_name}/dumps/\
{atom_info[0]}.eq.dump",
    )
    else:
        dump(
            file,
            f"{dir_name}/dumps/\
{atom_info[0]}.eq.dump",
        )
    thermo(file, 10)
    run(file, n_runs)
    write_data(
        file,
        f"{dir_name}/data/\
{atom_info[0]}.eq.data",
    )


def create_atoms(file, atom_info):
    file.write(f"lattice {atom_info[1]} {atom_info[2]}{atom_info[3]}\n")
    if atom_info[0] == "SiC":
        file.write(
            "create_atoms 1 region r1 &\n\
basis 1 2 &\nbasis 2 2 &\n\
basis 3 2 &\nbasis 4 2 &\n\
basis 5 1 &\nbasis 6 1 &\n\
basis 7 1 &\nbasis 8 1 &\n"
        )
    elif atom_info[0] == "FeNi":
        file.write(
            "create_atoms 1 region r1 &\n\
basis 1 1 &\nbasis 2 2 &\n"
        )
    elif atom_info[0] == "HG":
        file.write(
            "create_atoms 1 region r1 &\n\
basis 1 1 &\n\
basis 2 1 &\n\
basis 3 1 &\n\
basis 4 1 &\n\
basis 5 1 &\n\
basis 6 1 &\n\
basis 7 1 &\n\
basis 8 1 &\n\
basis 9 1 &\n\
basis 10 1 &\n\
basis 11 1 &\n\
basis 12 1 &\n\
basis 13 1 &\n\
basis 14 1 &\n\
basis 15 1 &\n\
basis 16 1 &\n\
basis 17 2 &\n"
        )
    elif atom_info[0] == "HG8":
        file.write(
            "create_atoms 1 region r1 &\n\
basis 1 1 &\n\
basis 2 1 &\n\
basis 3 1 &\n\
basis 4 1 &\n\
basis 5 1 &\n\
basis 6 1 &\n\
basis 7 1 &\n\
basis 8 1 &\n\
basis 9 2 &\n"
        )
    elif atom_info[0] == "HG2":
        file.write(
            "create_atoms 1 region r1 &\n\
basis 1 1 &\n\
basis 2 1 &\n\
basis 3 1 &\n\
basis 4 1 &\n\
basis 5 1 &\n\
basis 6 1 &\n\
basis 7 1 &\n\
basis 8 1 &\n\
basis 9 2 &\n\
basis 10 2 &\n"
        )
    elif atom_info[0] == "HG32":
        file.write(
            "create_atoms 1 region r1 &\n\
basis 1 1 &\n\
basis 2 1 &\n\
basis 3 1 &\n\
basis 4 1 &\n\
basis 5 1 &\n\
basis 6 1 &\n\
basis 7 1 &\n\
basis 8 1 &\n\
basis 9 1 &\n\
basis 10 1 &\n\
basis 11 1 &\n\
basis 12 1 &\n\
basis 13 1 &\n\
basis 14 1 &\n\
basis 15 1 &\n\
basis 16 1 &\n\
basis 17 1 &\n\
basis 18 1 &\n\
basis 19 1 &\n\
basis 20 1 &\n\
basis 21 1 &\n\
basis 22 1 &\n\
basis 23 1 &\n\
basis 24 1 &\n\
basis 25 1 &\n\
basis 26 1 &\n\
basis 27 1 &\n\
basis 28 1 &\n\
basis 29 1 &\n\
basis 30 1 &\n\
basis 31 1 &\n\
basis 32 1 &\n\
basis 33 2 &\n"
        )
    else:
        file.write("create_atoms 1 region r1\n")


def array_of_lists_to_array(arr):
    return np.apply_along_axis(
        lambda a: np.array(a[0], dtype=str), -1, arr[..., None]
    )


def create_groups(file, box):
    file.write("\n")
    for i in range(len(box)):
        file.write("group g{} region r{}\n".format(i + 1, i + 1))


def pair_style(file, pot, atom_types, types):
    file.write("\n")
    if len(pot) > 1:
        pair_styles = []
        pair_style = "hybrid"
        for i in range(len(pot)):
            pair_styles.append(f"{pot[i, 1]}")
        str_styles = " ".join(pair_styles)
        file.write(f"pair_style {pair_style} {str_styles}\n")
        n_styles = {}
        for i in range(len(pair_styles)):
            if pair_styles[i] not in n_styles.keys():
                n_styles[pair_styles[i]] = [1, 1]
            else:
                n_styles[pair_styles[i]][0] += 1
        for i in range(len(pot)):
            atom_list = np.full(atom_types, "NULL", dtype="U100")
            index = pot[i, 0].split("-")
            for j in range(int(len(index) / 2)):
                atom_list[int(index[j * 2])] = pot[i, 5]
                atom_list[int(index[j * 2 + 1])] = pot[i, 6]
            mat_str = " ".join(atom_list)
            if n_styles[pair_styles[i]][0] != 1:
                if pair_styles[i] == "meam":
                    file.write(
                        f"pair_coeff * * \
{pair_styles[i]} {n_styles[pair_styles[i]][1]} {pot[i, 2]} {pot[i, 3]} \
{pot[i, 4]} {mat_str}\n"
                    )
                elif pair_styles[i] == "bop":
                    file.write(
                        f"pair_coeff * * \
{pair_styles[i]} {n_styles[pair_styles[i]][1]} {pot[i, 2]} {mat_str}\n"
                    )
                    file.write(f"comm_modify cutoff {12}\n")
                elif pair_styles[i] == "tersoff":
                    file.write(
                        f"pair_coeff * * \
{pair_styles[i]} {n_styles[pair_styles[i]][1]} {pot[i, 2]} {mat_str}\n"
                    )
                n_styles[pair_styles[i]][1] += 1
            else:
                if pair_styles[i] == "meam":
                    file.write(
                        f"pair_coeff * * \
{pair_styles[i]} {pot[i, 2]} {pot[i, 3]} {pot[i, 4]} {mat_str}\n"
                    )
                elif pair_styles[i] == "bop":
                    file.write(
                        f"pair_coeff * * \
{pair_styles[i]} {pot[i, 2]} {mat_str}\n"
                    )
                    file.write(f"comm_modify cutoff {12}\n")
                elif pair_styles[i] == "tersoff":
                    file.write(
                        f"pair_coeff * * \
{pair_styles[i]} {pot[i, 2]} {mat_str}\n"
                    )
    else:
        if atom_types == 1:
            pair_style = pot[0, 1]
            file.write(f"pair_style {pair_style}\n")
            if pair_style == "meam":
                file.write(
                    f"pair_coeff * * {pot[0, 2]} {pot[0, 3]} {pot[0, 4]} \
{pot[0, 5]}\n"
                )
            elif pair_style == "bop":
                file.write(f"pair_coeff * * {pot[0, 2]} {pot[0, 5]}\n")
                file.write(f"comm_modify cutoff {12}\n")
            elif pair_style == "tersoff":
                file.write(f"pair_coeff * * {pot[0, 2]} {pot[0,5]}\n")
        else:
            if len(types) == 1:
                atom_list = np.full(atom_types, "NULL", dtype="U100")
                pair_style = pot[0, 1]
                index = pot[0, 0].split("-")
                for j in range(int(len(index) / 2)):
                    atom_list[int(index[j * 2])] = pot[0, 5]
                    atom_list[int(index[j * 2 + 1])] = pot[0, 6]
                mat_str = " ".join(atom_list)
                file.write(f"pair_style {pair_style}\n")
                if pair_style == "meam":
                    file.write(
                        f"pair_coeff * * {pot[0, 2]} {pot[0, 3]} {pot[0, 4]} \
{mat_str}\n"
                    )
                elif pair_style == "bop":
                    file.write(f"pair_coeff * * {pot[0, 2]} {mat_str}\n")
                    file.write(f"comm_modify cutoff {12}\n")
                elif pair_style == "tersoff":
                    file.write(f"pair_coeff * * {pot[0, 2]} {mat_str}\n")
            else:
                atom_list = np.full(atom_types, "NULL", dtype="U100")
                pair_style = pot[0, 1]
                index = pot[0, 0].split("-")
                for j in range(int(len(index) / 2)):
                    atom_list[int(index[j * 2])] = pot[0, 5]
                    atom_list[int(index[j * 2 + 1])] = pot[0, 6]
                mat_str = " ".join(atom_list)
                file.write(f"pair_style {pair_style}\n")
                if pair_style == "meam":
                    file.write(
                        f"pair_coeff * * {pot[0, 2]} {pot[0, 3]} {pot[0, 4]} \
{mat_str}\n"
                    )
                elif pair_style == "bop":
                    file.write(f"pair_coeff * * {pot[0, 2]} {mat_str}\n")
                    file.write(f"comm_modify cutoff {12}\n")
                elif pair_style == "tersoff":
                    file.write(f"pair_coeff * * {pot[0, 2]} {mat_str}\n")


def fix_nve(file, fix_number, group_number):
    file.write(f"\nfix f{fix_number} g{group_number} nve\n")


def fix_nvt(file, fix_number, group_number, t0, t1):
    file.write(
        f"\nfix f{fix_number} g{group_number} nvt temp {t0} {t1} 0.1\n"
    )


def compute_energy(file, list_print):
    file.write("compute ke all ke \ncompute pe all pe\n")
    list_print.append(["$(c_ke)", "Kinetic-energy"])
    list_print.append(["$(c_pe)", "Potential-energy"])


def compute_T_partial(file, moving_regions, list_print):
    for r in moving_regions:
        file.write(f"compute T{r.name} g{r.name} temp/partial 0 1 1\n")
        if r.name == 3 or r.name == 4:
            list_print.append([f"$(c_T{r.name})", f"T_{r.atom_type}_s"])
        else:
            list_print.append([f"$(c_T{r.name})", f"T_{r.atom_type}"])


def compute_T(file, moving_regions, list_print):
    file.write("\n")
    for r in moving_regions:
        file.write(f"compute T_{r.name} g{r.name} temp\n")
        list_print.append([f"$(c_T{r.name})", f"T_{r.atom_type}"])


def dump(file, dump_name):
    file.write(
        f"\ndump d1 all custom 10 {dump_name} id type x y z vx vy vz \
fx fy fz\n"
    )
    file.write("dump_modify d1 sort id\n")


def thermo(file, n_step):
    file.write(f"\nthermo {n_step}\n")


def run(file, number):
    file.write(f"\nrun {number}")


def write_data(file, data_name):
    file.write(f"\nwrite_data {data_name}")


def mass(file, atom_info, atom_types):
    file.write("\n")
    if atom_types == 1:
        file.write(f"mass {1} {atom_info[4]}\n")
    else:
        if atom_info.shape == (6,):
            i = 0
            masses = atom_info[4].split(" ")
            for mass in masses:
                file.write(f"mass {i+1} {mass}\n")
                i += 1
        else:
            i = 0
            for atom in atom_info:
                if atom[4] == 1:
                    file.write(f"mass {i+1} {atom[4]}\n")
                    i += 1
                else:
                    masses = atom[4].split(" ")
                    for mass in masses:
                        file.write(f"mass {i+1} {mass}\n")
                        i += 1


def fix_print(file, temp_name, list_print):
    list_print = np.array(list_print, dtype=str)
    compute_str = " ".join(list_print[:, 0])
    title_str = " ".join(list_print[:, 1])
    file.write(
        f'fix f2 all print 10 "{compute_str}" file {temp_name} title \
"{title_str}"\n'
    )


def fix_move_linear(file, vel):
    file.write("fix f3 g1 move linear 0 0 0 units box\n")
    file.write("fix f4 g8 move linear 0 0 0 units box\n")
    file.write("fix f5 g9 move linear 0 0 0 units box\n")
    file.write(f"fix f6 g2 move linear {vel[0]} {vel[1]} {vel[2]} units box\n")
    file.write(f"fix f7 g10 move linear {vel[0]} {vel[1]} {vel[2]} units box\n")
    file.write(
        f"fix f8 g11 move linear {vel[0]} {vel[1]} {vel[2]} units box\n"
    )
    file.write("fix f9 g12 move linear 0 0 0 units box\n")
    file.write("fix f10 g13 move linear 0 0 0 units box\n")
    file.write(f"fix f11 g14 move linear {vel[0]} {vel[1]} {vel[2]} units box\n")
    file.write(f"fix f12 g15 move linear {vel[0]} {vel[1]} {vel[2]} units box\n")
