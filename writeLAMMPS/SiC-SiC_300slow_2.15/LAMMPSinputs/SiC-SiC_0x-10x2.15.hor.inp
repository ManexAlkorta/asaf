# This script loads the .data files of two system to simulate the friction between them. In this case, it loads
# a SiC-SiC system with 2.15 # Angstroms between them. The velocity of the system is 0
# Angstroms/fs.
units metal
boundary s s s
atom_style atomic

read_data SiC-SiC_300slow_2.15/data/SiC.eq.data extra/atom/types 2 group g16
read_data SiC-SiC_300slow_2.15/data/SiC.eq.data add append offset 2 2 2 2 2 shift 0 -10 2.15 group g17

displace_atoms g16 rotate 29.85 0 0 0 1 0 180

region r1 block INF INF INF INF -39.9 -27.9 units box
region r2 block INF INF INF INF 30.049999999999997 42.05 units box
region r3 block INF INF INF INF -5.0 0.0 units box
region r4 block INF INF INF INF 2.15 7.15 units box
region r5 block INF INF INF INF -27.9 -5.0 units box
region r6 block INF INF INF INF 7.15 30.049999999999997 units box
region r7 block INF INF INF INF -32.9 35.05 units box
region r8 block INF INF -10.0 2.0 -39.9 0.0 units box
region r9 block INF INF 77.6 89.6 -39.9 0.0 units box
region r10 block INF INF -20.0 -8.0 2.15 42.05 units box
region r11 block INF INF 67.6 79.6 2.15 42.05 units box
region r12 block -10.0 2.0 INF INF -39.9 0.0 units box
region r13 block 57.7 69.7 INF INF -39.9 0.0 units box
region r14 block -10.0 2.0 INF INF 2.15 42.05 units box
region r15 block 57.7 69.7 INF INF 2.15 42.05 units box

group g1 region r1
group g2 region r2
group g3 region r3
group g4 region r4
group g5 region r5
group g6 region r6
group g7 region r7
group g8 region r8
group g9 region r9
group g10 region r10
group g11 region r11
group g12 region r12
group g13 region r13
group g14 region r14
group g15 region r15

pair_style tersoff
pair_coeff * * ../potentials/SiC.tersoff Si C Si C

mass 1 28.05
mass 2 12.01
mass 3 28.05
mass 4 12.01

timestep 0.001

neighbor 0.3 bin
neigh_modify delay 5

fix f1 g7 nve
compute ke all ke 
compute pe all pe
compute T3 g3 temp/partial 0 1 1
compute T4 g4 temp/partial 0 1 1
compute T5 g5 temp/partial 0 1 1
compute T6 g6 temp/partial 0 1 1
fix f2 all print 100 "$(c_ke) $(c_pe) $(c_T3) $(c_T4) $(c_T5) $(c_T6)" file SiC-SiC_300slow_2.15/parameters/SiC-SiC_0x-10x2.15.temp title "Kinetic-energy Potential-energy T_SiC_s T_SiC_s T_SiC T_SiC"
fix f3 g1 move linear 0 0 0 units box
fix f4 g8 move linear 0 0 0 units box
fix f5 g9 move linear 0 0 0 units box
fix f6 g2 move linear 0 0.5 0 units box
fix f7 g10 move linear 0 0.5 0 units box
fix f8 g11 move linear 0 0.5 0 units box
fix f9 g12 move linear 0 0 0 units box
fix f10 g13 move linear 0 0 0 units box
fix f11 g14 move linear 0 0.5 0 units box
fix f12 g15 move linear 0 0.5 0 units box

dump d1 all custom 100 SiC-SiC_300slow_2.15/dumps/SiC-SiC_0x-10x2.15.dump id type x y z vx vy vz fx fy fz
dump_modify d1 sort id

thermo 10

run 79599