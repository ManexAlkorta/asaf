# This script loads the .data files of two system to simulate the friction between them. In this case, it loads
# a HG8-HG8 system with 4 # Angstroms between them. The velocity of the system is 0
# Angstroms/fs.
units metal
boundary s s s
atom_style atomic

read_data HG8_eq/data/HG8.eq.data extra/atom/types 2 group g16
read_data HG8_eq/data/HG8.eq.data add append offset 2 2 2 2 2 shift 0 -2.5 4 group g17

displace_atoms g16 rotate 25.0 0 0 0 1 0 180

region r1 block INF INF INF INF -14.0 0.0 units box
region r2 block INF INF INF INF 4.0 18.0 units box
region r3 block INF INF INF INF -2.0 0.0 units box
region r4 block INF INF INF INF 4.0 4.0 units box
region r5 block INF INF INF INF 0.0 0.0 units box
region r6 block INF INF INF INF 4.0 4.0 units box
region r7 block INF INF INF INF 0.0 4.0 units box
region r8 block INF INF -10.0 4.0 -14.0 0.0 units box
region r9 block INF INF 46.0 60.0 -14.0 0.0 units box
region r10 block INF INF -12.5 1.5 4.0 18.0 units box
region r11 block INF INF 43.5 57.5 4.0 18.0 units box
region r12 block -10.0 4.0 INF INF -14.0 0.0 units box
region r13 block 48.0 60.0 INF INF -14.0 0.0 units box
region r14 block -10.0 4.0 INF INF 4.0 18.0 units box
region r15 block 48.0 60.0 INF INF 4.0 18.0 units box

group g1 region r1
group g2 region r2
group g3 region r3
group g4 region r4
group g5 region r5
group g6 region r6
group g7 region r7
group g8 region r8
group g9 region r9
group g10 region r10
group g11 region r11
group g12 region r12
group g13 region r13
group g14 region r14
group g15 region r15

pair_style mlip ../potentials/HG_def_1_dup.ini
pair_coeff * *

mass 1 12.011
mass 2 1.00784
mass 3 12.011
mass 4 1.00784

timestep 0.001

neighbor 0.3 bin
neigh_modify delay 5

fix f1 g7 nve
compute ke all ke 
compute pe all pe
compute T3 g3 temp/partial 0 1 1
compute T4 g4 temp/partial 0 1 1
compute T5 g5 temp/partial 0 1 1
compute T6 g6 temp/partial 0 1 1
fix f2 all print 10 "$(c_ke) $(c_pe) $(c_T3) $(c_T4) $(c_T5) $(c_T6)" file HG8_eq/parameters/HG8-HG8_0x-2.5x4.temp title "Kinetic-energy Potential-energy T_HG8_s T_HG8_s T_HG8 T_HG8"
fix f3 g1 move linear 0 0 0 units box
fix f4 g8 move linear 0 0 0 units box
fix f5 g9 move linear 0 0 0 units box
fix f6 g2 move linear 0 1 0 units box
fix f7 g10 move linear 0 1 0 units box
fix f8 g11 move linear 0 1 0 units box
fix f9 g12 move linear 0 0 0 units box
fix f10 g13 move linear 0 0 0 units box
fix f11 g14 move linear 0 1 0 units box
fix f12 g15 move linear 0 1 0 units box

dump d1 all custom 10 HG8_eq/dumps/HG8-HG8_0x-2.5x4.dump id type x y z vx vy vz fx fy fz
dump_modify d1 sort id

thermo 10

run 10000