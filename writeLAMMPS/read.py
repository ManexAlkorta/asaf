"""This module reads the inputs files for the LAMMPS friction simulation."""

import numpy as np

path = "inputs/saia.inp"


mat = np.empty(2, dtype="U100")
method = "hor"
units = "metal"
boundary = ["s", "s", "s"]
atom_style = "atomic"
disp = [0, 0, 10]
vel = [1, 0, 0]
d_freeze = 4


class SimParams:
    def __init__(self):
        self.units = "metal"
        self.boundary = ["s", "s", "s"]
        self.atom_style = "atomic"
        self.method = "eq-hor"
        self.matA = "Fe"
        self.matB = "SiC"
        self.timestep = [0.001,0.001]
        self.n_runs = [5000, 5000]
        self.h_runs = 10000
        self.exe = "lmp_stable"
        self.box = np.array([10, 10, 10])
        self.disp = np.array([-10, 0, 4])
        self.d_freeze = 2
        self.EnableMovie = False
        self.mlip=False
        self.potential=''
        self.potential_dup=''
        self.dir_name="simulation"
        self.is_hruns=False

    def set_method(self, method):
        self.method = method

    def set_matA(self, matA):
        self.matA = matA

    def set_matB(self, matB):
        self.matB = matB

    def set_timestep(self, i, timestep):
        self.timestep[i] = float(timestep)

    def set_nruns(self, i, n_runs):
        self.n_runs[i] = n_runs

    def set_box(self, box):
        self.box = box
        self.box_size = np.array(
            [
                box[0, 1] - box[0, 0],
                box[1, 1] - box[1, 0],
                box[2, 1] - box[2, 0],
            ]
        )
    
    def set_hruns(self, h_runs):
        self.h_runs=h_runs

    def set_lmp_exe(self, lmp_executable):
        self.exe = lmp_executable

    def set_distance(self, distance):
        self.disp = distance

    def set_dfreeze(self, d_freeze):
        self.d_freeze = d_freeze

    def set_vel(self, vel):
        self.vel = vel

    def set_atomic_info(self, atomic_info):
        self.atomic_info = atomic_info

    def set_atom_info(self, atom_info):
        self.atom_info = atom_info

    def set_mass(self, mass):
        self.mass = mass

    def set_box_info(self, box_info):
        self.box_info = box_info

    def set_lammps_exe(self, lammps_exe):
        self.exe = lammps_exe
    
    def set_temp(self, temp):
        self.temp = temp

    def set_dir(self, dir_name):
        self.dir_name=dir_name

    def set_movie_params(self, format, angles, elev, color_mode):
        self.MovieEnable = True
        self.MovieFormat = format
        try:
            self.MovieAngles = [
                float(angles.split(" ")[0]),
                float(angles.split(" ")[1]),
            ]
        except IndexError:
            self.MovieAngles = [angles, angles]
        self.MovieElev = float(elev)
        self.MovieColormode = color_mode
    def set_potential(self, potential, potential_dup):
        self.mlip=True
        self.potential=potential
        self.potential_dup=potential_dup


def read(path):
    data = np.loadtxt(path, delimiter="\n", dtype=str)
    simulation = SimParams()
    i = 0
    while i < len(data):
        if data[i] == "method":
            simulation.set_method(data[i + 1])
            i += 2
        elif data[i] == "matA":
            simulation.set_matA(data[i + 1])
            i += 2
        elif data[i] == "matB":
            simulation.set_matB(data[i + 1])
            i += 2
        elif data[i] == "mlip_pot":
            simulation.set_potential(data[i+1],data[i+2])
            i += 3
        elif data[i] == "timestepA":
            simulation.set_timestep(0, data[i + 1])
            i+=2
        elif data[i] == "timestepB":
            simulation.set_timestep(1, data[i + 1])
            i += 2
        elif data[i] == "n_runsA":
            simulation.set_nruns(0, data[i + 1])
            i += 2
        elif data[i] == "n_runsB":
            simulation.set_nruns(1, data[i + 1])
            i += 2
        elif data[i] == "n_runs":
            simulation.set_hruns(int(data[i+1]))
            simulation.is_hruns=True
            i+=2
        elif data[i] == "box":
            box_info = data[i + 1].split(" ")
            box_size = np.array(box_info, dtype=float)
            simulation.set_box(
                np.array(
                    [[0, box_size[0]], [0, box_size[1]], [0, box_size[2]]]
                )
            )
            i += 2
        elif data[i] == "distance":
            simulation.set_distance(data[i + 1].split(" "))
            i += 2
        elif data[i] == "vel":
            simulation.set_vel(data[i + 1].split(" "))
            i += 2
        elif data[i] == "d_freeze":
            simulation.set_dfreeze(float(data[i + 1]))
            i += 2
        elif data[i] == "temperature":
            simulation.set_temp(float(data[i+1]))
            i += 2
        elif data[i] == "lammps_exe":
            simulation.set_lammps_exe(data[i + 1])
            i += 2
        elif data[i] == "dir_name":
            simulation.set_dir(data[i+1])
            i+=2
        elif data[i] == "movie_parameters":
            i += 1
            elev, angles, format, color_mode, i = read_movie_parameters(
                data, i
            )
            simulation.set_movie_params(format, angles, elev, color_mode)
    return simulation


def read_movie_parameters(data, i):
    read_movie = True
    while read_movie and i < len(data):
        if data[i] == "format":
            format = data[i + 1]
            i += 2
        elif data[i] == "angles":
            angles = data[i + 1]
            i += 2
        elif data[i] == "elev":
            elev = data[i + 1]
            i += 2
        elif data[i] == "color_mode":
            color_mode = data[i + 1]
            i += 2
        else:
            read_movie = False
    return (elev, angles, format, color_mode, i)


if __name__ == "__main__":
    read(path)
