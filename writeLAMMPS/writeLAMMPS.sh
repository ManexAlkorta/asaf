export PYTHONPATH="$(echo $(cd ../ && pwd)):$PYTHONPATH"
echo "backend: Agg" > ~/.config/matplotlib/matplotlibrc
python3 write_friction.py $1
rm ~/.config/matplotlib/matplotlibrc

