"""This module runs a friction simulation using LAMMPS.

The supported materials are Cu, Fe and SiC. An example for the input file:
-------------------------------------------------------------------------
    method
    eq-hor

    matA
    Fe

    matB
    Fe

    box
    10 10 10

    timestep
    0.001

    vel
    1 0 0

    distance
    0 0 10

    d_freeze
    10

    n_runs
    5000
-------------------------------------------------------------------------

Method: This keyword sets the functionality of the simulation.
        eq: You can use to equilibrate one or two systems.
        hor: It uses the .data files from the previous eq simulation to run
            the horizontal motion.
        eq-hor: Runs both simulations.

matA, matB:  The atom types of the slides. Supported atoms are Cu, Fe and SiC.
    To use SiC with another material, use it as matB.

box: This keyword defines the box size. Define each axis size separated by
    spaces. For example: 10 10 10.
    Units: Angstroms.

timestep: This keyword sets the timestep to use in the simulation.
    Units: picoseconds.

vel: This keyword defines the velocity of the upper slide.
    Units: Angstroms/fentasecond

d_freeze: This keyword defines the depth of the frozen region in z axis.
    Units: Angstroms

n_runs: This keyword defines the number of runs to use in the equilibration of
    the system. The runs for the horizontal motion are calculated using the box
    size and velocity.

--------------------------------------------------------------------------
"""


import numpy as np
import writeLAMMPS.write as wr
import writeLAMMPS.read as rd
import graphs.fun as fun
import subprocess as sp
import ast
import sys
import time

path = sys.argv[1]
# Reading the information for the potentials.
file = open("parameters/potentials_dict", "r")
contents = file.read()
potentials = ast.literal_eval(contents)
file.close()
# Reading the information for the atomic parameters.
file = open("parameters/lattice_constants_dict", "r")
latt_constants = ast.literal_eval(file.read())
file.close()


class region:
    """Region class is used to create atom groups in the simulation.

    The region class contains information about the atoms filling the region.
    The groups are needed to compute some parameters as the energy or the
    temperature, or to implement the motion for the friction.
    """

    def __init__(self, name: str, atom_type: str, box_info: np.ndarray):
        self.name = name
        self.xlo = box_info[0, 0]
        self.xhi = box_info[0, 1]
        self.ylo = box_info[1, 0]
        self.yhi = box_info[1, 1]
        self.zlo = box_info[2, 0]
        self.zhi = box_info[2, 1]
        self.box = np.array(
            [[self.xlo, self.xhi], [self.ylo, self.yhi], [self.zlo, self.zhi]],
            dtype=float,
        )
        self.atom_type = atom_type

    def displ_box(self, displ: np.ndarray):
        """This function displaces all the atoms of a region.

        Args:
            displ: The distance in angstroms to move the region. np.ndarray
        """
        self.xlo += float(displ[0])
        self.xhi += float(displ[0])
        self.ylo += float(displ[1])
        self.yhi += float(displ[1])
        self.zlo += float(displ[2])
        self.zhi += float(displ[2])
        self.box = np.array(
            [[self.xlo, self.xhi], [self.ylo, self.yhi], [self.zlo, self.zhi]],
            dtype=float,
        )


def get_atom_info(mat: list) -> np.ndarray:
    """This function gives the structures the information of the atom-types.

    Args:
        mat: A list containing the atom-types used in the simulation.

    Returns:
        atomic_info: The information needed in the simulation for each
            atom-type. np.ndarray
    """
    atomic_info = np.empty([len(mat), 6], dtype="U1000")
    atom_types = []
    for i in range(len(mat)):
        if mat[i] == "SiC":
            atom_types.append("Si")
            atom_types.append("C")
        else:
            atom_types.append(mat[i])
        atomic_info[i, 0] = mat[i]
        atomic_info[i, 1] = latt_constants[mat[i]][0]
        # Crystal structure: str
        atomic_info[i, 2] = latt_constants[mat[i]][1]
        # lattice constant: float
        atomic_info[i, 3] = latt_constants[mat[i]][2]
        # Customized structure if needed. Else empty.
        atomic_info[i, 4] = latt_constants[mat[i]][3]
        # Mass. float.
        atomic_info[i, 5] = latt_constants[mat[i]][4]
        # Number of elements. int.
    return atomic_info


def frozen_regions(
    atomic_info: np.ndarray,
    box_size: np.ndarray,
    disp: np.ndarray,
    d_freeze: np.ndarray,
):
    """This functions creates the frozen regions.

    Args:
        atom-info: The information needed in the simulation for each atom-type.
            np.ndarray.
        box-size: The size of each axis of the slide to split.
            np.ndarray.
        disp: The distance between the slides. np.ndarray.
        d_freeze: The depth of the frozen regions. float

    Returns:
        frozen_region: A list of the frozen regions. list of regions.
        box: A list containing the box of all the regions. Is going to be used
            also for the moving regions. list of np.ndarray.
    """
    box = np.empty([15, 3, 2], dtype="U100")
    f1 = region(
        1,
        atomic_info[0, 0],
        np.array(
            [
                ["INF", "INF"],
                ["INF", "INF"],
                [
                    -box_size[2]-10,
                    -box_size[2] + d_freeze,
                ],
            ]
        ),
    )
    box[0] = f1.box
    f2 = region(
        2,
        atomic_info[1, 0],
        np.array(
            [
                ["INF", "INF"],
                ["INF", "INF"],
                [
                    box_size[2] - d_freeze + float(disp[2]),
                    box_size[2] + float(disp[2])+10,
                ],
            ]
        ),
    )
    box[1] = f2.box
    f3 = region(
        3,
        atomic_info[1, 0],
        np.array(
            [
                ["INF", "INF"],
                [-10, d_freeze],
                [-box_size[2]-10, 0],
            ]
        ),
    )
    box[7] = f3.box
    f4 = region(
        4,
        atomic_info[1, 0],
        np.array(
            [
                ["INF", "INF"],
                [box_size[1] - d_freeze, box_size[1]+10],
                [-box_size[2]-10, 0],
            ]
        ),
    )
    box[8] = f4.box
    f5 = region(
        5,
        atomic_info[1, 0],
        np.array(
            [
                ["INF", "INF"],
                [-10+float(disp[1]), float(disp[1]) + d_freeze],
                [float(disp[2]), box_size[2]+float(disp[2])+10],
            ]
        ),
    )
    box[9] = f5.box
    f6 = region(
        6,
        atomic_info[1, 0],
        np.array(
            [
                ["INF", "INF"],
                [box_size[1] + float(disp[1]) - d_freeze, box_size[1] + 10 + float(disp[1])],
                [float(disp[2]), box_size[2]+float(disp[2])+10],
            ]
        ),
    )
    box[10] = f6.box
    f7 = region(
        7,
        atomic_info[1, 0],
        np.array(
            [
                [-10, d_freeze],
                ["INF", "INF"],
                [-box_size[2]-10, 0],
            ]
        ),
    )
    box[11] = f7.box
    f8 = region(
        8,
        atomic_info[1, 0],
        np.array(
            [
                [box_size[0]-2, box_size[0]+10],
                ["INF", "INF"],
                [-box_size[2]-10, 0],
            ]
        ),
    )
    box[12] = f8.box
    f9 = region(
        9,
        atomic_info[1, 0],
        np.array(
            [
                [-10, d_freeze],
                ["INF", "INF"],
                [float(disp[2]), box_size[2]+float(disp[2])+10],
            ]
        ),
    )
    box[13] = f9.box
    f10 = region(
        10,
        atomic_info[1, 0],
        np.array(
            [
                [box_size[0]-2, box_size[0]+10],
                ["INF", "INF"],
                [float(disp[2]), box_size[2]+float(disp[2])+10],
            ]
        ),
    )
    box[14] = f10.box
    return ([f1, f2, f3, f4, f5, f6, f7, f8, f9, f10], box)


def horizontal_motion(
    SimParams: rd.SimParams, box: np.ndarray
):
    """This function creates the remaining regions for the horizontal simulation.

    It takes the box from the frozen_regions() functions, and it's fill it
    with the moving ones. Also creates a list of moving regions from wich we
    can compute parameters in the future.

    Args:
        SimParams: The object containing the simulation parameters.
        box: A list containg the box of each region. list of np.ndarray.

    Returns:
        moving_regions: A list of the moving regions. list of regions
        box: A list containing the box of each region. list of np.ndarray.
    """
    moving_regions = []
    s1 = region(
        3,
        SimParams.atomic_info[0, 0],
        np.array([["INF", "INF"], ["INF", "INF"], [-2, 0]]),
    )
    moving_regions.append(s1)
    box[2] = s1.box
    s2 = region(
        4,
        SimParams.atomic_info[1, 0],
        np.array(
            [
                ["INF", "INF"],
                ["INF", "INF"],
                [float(SimParams.disp[2]), float(SimParams.disp[2])],
            ]
        ),
    )
    moving_regions.append(s2)
    box[3] = s2.box
    b1 = region(
        5,
        SimParams.atomic_info[0, 0],
        np.array(
            [
                ["INF", "INF"],
                ["INF", "INF"],
                [-SimParams.box_size[2] + SimParams.d_freeze, 0],
            ]
        ),
    )
    moving_regions.append(b1)
    box[4] = b1.box
    b2 = region(
        6,
        SimParams.atomic_info[1, 0],
        np.array(
            [
                ["INF", "INF"],
                ["INF", "INF"],
                [
                    float(SimParams.disp[2]),
                    SimParams.box_size[2]
                    + float(SimParams.disp[2])
                    - SimParams.d_freeze,
                ],
            ]
        ),
    )
    moving_regions.append(b2)
    box[5] = b2.box
    moving_atoms = region(
        7,
        "mix",
        np.array(
            [
                ["INF", "INF"],
                ["INF", "INF"],
                [
                    -SimParams.box_size[2] + SimParams.d_freeze,
                    SimParams.box_size[2]
                    - SimParams.d_freeze
                    + float(SimParams.disp[2]),
                ],
            ]
        ),
    )
    moving_regions.append(moving_atoms)
    box[6] = moving_atoms.box
    return (moving_regions, np.char.upper(box))


def create_potentials(
    atom_types: np.ndarray, types: np.ndarray, types_ext: np.ndarray
) -> np.ndarray:
    """This function structures the needen information for the pair interactions.

    It creates a np.ndarray from which to write the LAMMPS input.

    Args:
        atom_types: The number of atom types used in the simulation. int.
        types: The atom_types used in the simulation. list of str.
        types_ext: The atom types used in the simulation, if the same atom
            type is used twice in the same simulation it is also considered.
            This is needed because in LAMMPS we load separately the
            information of each slide. list of str.

    Returns:
        pot: An array that contains the needed information for the pair
            interactions. np.ndarray.

    """
    if len(types) == 1:
        # If both slides have the same material, we only need one potential.
        pot = np.empty([1, 8], dtype="U100")
        if f"{types[0]}-{types[0]}" in potentials.keys():
            key = f"{types[0]}-{types[0]}"
            pot[0, 0] = f"{0}-{1}"
            pot[0, 1] = potentials[key][0]
            pot[0, 2] = potentials[key][1]
            pot[0, 3] = potentials[key][2]
            pot[0, 4] = potentials[key][3]
            pot[0, 5] = potentials[key][4]
            pot[0, 6] = potentials[key][5]
            pot[0, 7] = key
        else:
            print("no potentials")
    else:
        # Depending on how many atom types we have, we give a defined dimension
        # to the array.
        if len(types) == 2:
            pot = np.empty([1, 8], dtype="U100")
        elif len(types) == 3:
            pot = np.empty([3, 8], dtype="U100")
        else:
            pot = np.empty(
                [np.math.factorial(atom_types - 1), 8], dtype="U100"
            )
        k = 0
        for i in range(len(types_ext)):
            for j in range(i + 1, len(types_ext)):
                # We make a loop to implement all the pair interactions just
                # once.
                if f"{types_ext[i]}-{types_ext[j]}" in potentials.keys():
                    # We look if this pair interaction is in the dictionary.
                    key = f"{types_ext[i]}-{types_ext[j]}"
                    if key in pot[:, 7]:
                        k += -1
                        pot[k, 0] += f"-{i}-{j}"
                        k += 1
                    elif types_ext[i] != types_ext[j]:
                        # We structure the needed info in a np.array.
                        pot[k, 0] = f"{i}-{j}"
                        pot[k, 1] = potentials[key][0]
                        pot[k, 2] = potentials[key][1]
                        pot[k, 3] = potentials[key][2]
                        pot[k, 4] = potentials[key][3]
                        pot[k, 5] = potentials[key][4]
                        pot[k, 6] = potentials[key][5]
                        pot[k, 7] = key
                        k += 1
                elif f"{types_ext[j]}-{types_ext[i]}" in potentials.keys():
                    key = f"{types_ext[j]}-{types_ext[i]}"
                    if key in pot[:, 7]:
                        k += -1
                        pot[k, 0] += f"-{j}-{i}"
                        k += 1
                    else:
                        pot[k, 0] = f"{j}-{i}"
                        pot[k, 1] = potentials[key][0]
                        pot[k, 2] = potentials[key][1]
                        pot[k, 3] = potentials[key][2]
                        pot[k, 4] = potentials[key][3]
                        pot[k, 5] = potentials[key][4]
                        pot[k, 6] = potentials[key][5]
                        pot[k, 7] = key
                        k += 1
                else:
                    print("no potentials")
    return pot


def eq(SimParams: rd.SimParams, partial):
    """This function equilibrates the system with an nvt simulation.

    The chosen material is heated until it reaches 310K. When the simulation is
    finished, it creates .eq.data file.

    Args:
        SimParams: The object containing the simulation parameters.
    Returns:
        None.
    """
    if partial:
        atom=Sim.atomic_info[0]
        atom_types, types, types_ext = get_atom_types(atom)
        pot = create_potentials(atom_types, types, types_ext)
        r1 = region(1, atom[0], SimParams.box)
        wr.write_script_eq(
            f"{Sim.dir_name}/LAMMPSinputs/\
{atom[0]}.eq.inp",
            Sim.units,
            Sim.boundary,
            Sim.atom_style,
            np.array([r1.box]),
            int(atom[5]),
            types,
            atom,
            pot,
            Sim.timestep[0],
            [r1],
            Sim.n_runs[0],
            Sim.disp,
            [Sim.matA, Sim.matB],
            Sim.temp,
            partial,
            Sim.mlip,
            Sim.potential,
            Sim.dir_name
        )
        if Sim.mlip:
            start = time.time()
            sp.run(
                f"bash /home/partners/manex/lmp-mlip/lammps-friction/run_mlip.sh\
<{Sim.dir_name}\
/LAMMPSinputs/{atom[0]}.eq.inp >{Sim.dir_name}\
/outputs/{atom[0]}.eq.out",
                shell=True,
            )
            duration = time.time() - start
        else:
            start = time.time()
            sp.run("export OMP_NUM_THREADS=12", shell=True)
            # lmp_stable executable must be run with OPEN MP.
            sp.run(
                f"{Sim.exe} <{Sim.dir_name}\
/LAMMPSinputs/{atom[0]}.eq.inp >{Sim.dir_name}\
/outputs/{atom[0]}.eq.out",
                shell=True,
            )
            duration = time.time() - start
        print(f"{atom[0]} system equilibrated. TIME: {duration}")
    else:
        i=0
        for atom in Sim.atomic_info:
            atom_types, types, types_ext = get_atom_types(atom)
            pot = create_potentials(atom_types, types, types_ext)
            r1 = region(1, atom[0], SimParams.box)
            wr.write_script_eq(
                f"{Sim.dir_name}/LAMMPSinputs/\
{atom[0]}.eq.inp",
                Sim.units,
                Sim.boundary,
                Sim.atom_style,
                np.array([r1.box]),
                int(atom[5]),
                types,
                atom,
                pot,
                Sim.timestep[i],
                [r1],
                Sim.n_runs[i],
                Sim.disp,
                [Sim.matA, Sim.matB],
                Sim.temp,
                partial,
                Sim.mlip,
                Sim.potential,
                Sim.dir_name
            )
            print(f"Equilibrating {atom[0]} system...")
            if Sim.mlip:
                start = time.time()
                sp.run(
                    f"bash /home/partners/manex/lmp-mlip/lammps-friction/run_mlip.sh\
 <{Sim.dir_name}\
/LAMMPSinputs/{atom[0]}.eq.inp >{Sim.dir_name}\
/outputs/{atom[0]}.eq.out",
                    shell=True,
                )
                duration = time.time() - start
            else:
                start = time.time()
                sp.run("export OMP_NUM_THREADS=12", shell=True)
                # lmp_stable executable must be run with OPEN MP.
                sp.run(
                    f"{Sim.exe} <{Sim.dir_name}\
/LAMMPSinputs/{atom[0]}.eq.inp >{Sim.dir_name}\
/outputs/{atom[0]}.eq.out",
                    shell=True,
                )
                duration = time.time() - start
            i+=1
            print(f"{atom[0]} system equilibrated. TIME: {duration}")


def hor(SimParams: rd.SimParams):
    """This functions runs the friction simulation.

    It takes the initial state from the .eq.data files. Afterwards, the
    friction is implemented using a nve simulation. Some parameters such
    as temperature as computed and saved in .temp files.

    Args:
        SimParams: The object containing the simulation parameters.
    Return:
        None.

    """
    fr_reg, box = frozen_regions(
        SimParams.atomic_info,
        SimParams.box_size,
        SimParams.disp,
        SimParams.d_freeze,
    )
    atom_types, types, types_ext = get_atom_types(SimParams.atomic_info)
    pot = create_potentials(atom_types, types, types_ext)
    moving_regions, box = horizontal_motion(SimParams, box)
    wr.write_script_horizontal(
        f"{Sim.dir_name}/LAMMPSinputs/\
{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.hor.inp",
        Sim.units,
        Sim.boundary,
        Sim.atom_style,
        box,
        SimParams.box_size,
        atom_types,
        types,
        SimParams.atomic_info,
        pot,
        min(SimParams.timestep),
        moving_regions,
        SimParams.disp,
        SimParams.vel,
        f"{Sim.dir_name}/parameters/\
{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.temp",
        f"{Sim.dir_name}/dumps/\
{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.dump",
        SimParams.h_runs,
        Sim.mlip,
        Sim.potential_dup,
        Sim.dir_name
    )
    print(
        f"The {Sim.matA}-{Sim.matB} friction simulation has started, this could \
take hours depending on the materials and the size of the slides ..."
    )
    if Sim.mlip:
        start = time.time()
        sp.run(
            f"bash /home/partners/manex/lmp-mlip/lammps-friction/run_mlip.sh \
<{Sim.dir_name}/\
LAMMPSinputs/{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}\
.hor.inp >{Sim.dir_name}/\
outputs/{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}\
.hor.out ",
            shell=True,
        )
        duration = time.time() - start
    else:
        start = time.time()
        sp.run("export OMP_NUM_THREADS=12", shell=True)
        sp.run(
            f"{Sim.exe} <{Sim.dir_name}/\
LAMMPSinputs/{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}\
.hor.inp >{Sim.dir_name}/\
outputs/{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}\
.hor.out ",
            shell=True,
        )
        duration = time.time() - start
    print(
        f"The {Sim.matA}-{Sim.matB} friction simulation has finished. TIME: \
{duration}"
    )


def get_atom_types(
    atomic_info: np.ndarray,
):
    """This function calculates the number of atom types used in the simulation.

    Args:
        atomic_info: The information of the atom types used in the simulation.
            np.ndarray.

    Returns:
        atom_types: The number of atom types used in the simulation. int.
        types: The atom_types used in the simulation. list of str.
        types_ext: The atom types used in the simulation, if the same atom
            type is used twice in the same simulation it is also considered.
            This is needed because in LAMMPS we load separately the
            information of each slide. list of str.

    """
    types = []
    types_ext = []
    atom_types = 0
    if atomic_info.shape == (6,):
        atom_types = int(atomic_info[5])
        if atomic_info[0] == "SiC":
            types.append("Si")
            types.append("C")
        else:
            types.append(atomic_info[0])
        types_ext = types
    else:
        for atom in atomic_info:
            atom_types += int(atom[5])
            if atom[0] not in types and int(atom[5]) == 1:
                types.append(atom[0])
                types_ext.append(atom[0])
            elif atom[0] == "SiC":
                if "Si" not in types:
                    types.append("Si")
                if "C" not in types:
                    types.append("C")
                types_ext.append("Si")
                types_ext.append("C")
            else:
                types_ext.append(atom[0])
    return (atom_types, types, types_ext)


if __name__ == "__main__":
    Sim = rd.read(path)
    Sim.set_atomic_info(get_atom_info([Sim.matA, Sim.matB]))
    if Sim.method == "eq":
        sp.run(
            f"mkdir {Sim.dir_name}",
            shell=True,
        )
        sp.run(
            f"mkdir {Sim.dir_name}/LAMMPSinputs \
{Sim.dir_name}/outputs \
{Sim.dir_name}/dumps \
{Sim.dir_name}/data \
{Sim.dir_name}/parameters",
            shell=True,
        )
        eq(Sim, partial=True)
    elif Sim.method == "hor":
        mass_list = []
        print(
            f"Starting the build up for the \
{Sim.matA}-{Sim.matB} friction simulation."
        )
        for atom in Sim.atomic_info:
            if atom[5] != 1:
                masses = atom[4].split(" ")
                for i in masses:
                    mass_list.append(float(i))
            else:
                mass_list.appned(float(i))
        if Sim.is_hruns != True:
            Sim.set_hruns(
                int(
                    (Sim.box_size[1]/2 - float(Sim.disp[0]))
                    / (float(Sim.vel[1]) * min(Sim.timestep))
                )
            )
        hor(Sim)
        fr_reg, box = frozen_regions(
        Sim.atomic_info,
        Sim.box_size,
        Sim.disp,
        Sim.d_freeze,
    )
        # The simulation is finished, and we start with the postproccesing.
        (
            timestep,
            n_atoms,
            n_frames,
            box_info,
            atom_info,
            r,
            r0,
        ) = fun.read_dump(
            f"{Sim.dir_name}/dumps/{Sim.matA}-\
{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.dump",
            ":",
        )
        Sim.set_box_info(box_info)
        Sim.set_atom_info(atom_info)
        Sim.set_mass(fun.get_mass(Sim.atom_info, mass_list))
        fun.view_distr(
            box_info,
            atom_info,
            d_freeze=10,
            disp=[float(Sim.disp[0]), float(Sim.disp[1]), float(Sim.disp[2])],
            path1=f"{Sim.dir_name}/parameters/\
forces.png",
        )
        s_bot, s_top = fun.create_surfaces(
            Sim.atom_info,
            s_depth=[Sim.box_size[0]/4, Sim.box_size[1]/4, Sim.box_size[2] - Sim.d_freeze],
            disp=[float(Sim.disp[0]), float(Sim.disp[1]), float(Sim.disp[2])],
        )
        #fr_coeff, fx, fy, fz = fun.get_friction(s_bot, atom_info)
        bottom=fun.create_bottom(np.array([
            [atom_info[:,:,2].min(), atom_info[:, :, 2].max()],
            [atom_info[:,:,3].min(), atom_info[:, :, 3].max()],
            [(fr_reg[0].box)[2,0],(fr_reg[0].box)[2,1]]],dtype=float),
            atom_info)
        
        fr_coeff, fx, fy, fz = fun.get_friction(bottom, atom_info)
        fun.save_friction_coeff(
            f"{Sim.dir_name}/parameters/\
friction.data",
            f"{Sim.dir_name}/parameters/\
{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.temp",
            timestep,
            fr_coeff,
            fy,
            fz,
        )
        fun.plot_friction_coeff(
            f"{Sim.dir_name}/parameters/\
friction.out",
            f"{Sim.dir_name}/parameters/\
friction.data",
            f"{Sim.dir_name}/parameters/\
forces_1.png",
            f"{Sim.dir_name}/parameters/\
friction.png",
            f"{Sim.dir_name}/parameters/\
fr_coeff.png"
        )
        fun.plot_parameters(
            f"{Sim.dir_name}/parameters/\
{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.temp",
            f"{Sim.dir_name}/parameters/\
parameters.png",
            timestep,
        )
    elif Sim.method == "eq-hor":
        mass_list = []
        print(
            f"Starting the build up for the \
{Sim.matA}-{Sim.matB} friction simulation."
        )
        for atom in Sim.atomic_info:
            if atom[5] != 1:
                masses = atom[4].split(" ")
                for i in masses:
                    mass_list.append(float(i))
            else:
                mass_list.appned(float(i))
        sp.run(
            f"mkdir {Sim.dir_name}",
            shell=True,
        )
        sp.run(
            f"mkdir \
{Sim.dir_name}/LAMMPSinputs \
{Sim.dir_name}/outputs \
{Sim.dir_name}/dumps \
{Sim.dir_name}/data \
{Sim.dir_name}/parameters",
            shell=True,
        )
        eq(Sim, partial=False)
        Sim.set_hruns(
            int(
                (Sim.box_size[1]/2 - float(Sim.disp[0]))
                / (float(Sim.vel[1]) * min(Sim.timestep))
            )
        )
        hor(Sim)
        fr_reg, box = frozen_regions(
        Sim.atomic_info,
        Sim.box_size,
        Sim.disp,
        Sim.d_freeze,
    )
        # The simulation is finished, and we start with the postproccesing.
        (
            timestep,
            n_atoms,
            n_frames,
            box_info,
            atom_info,
            r,
            r0,
        ) = fun.read_dump(
            f"{Sim.dir_name}/dumps/{Sim.matA}-\
{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.dump",
            ":",
        )
        Sim.set_box_info(box_info)
        Sim.set_atom_info(atom_info)
        Sim.set_mass(fun.get_mass(Sim.atom_info, mass_list))
        fun.view_distr(
            box_info,
            atom_info,
            d_freeze=10,
            disp=[float(Sim.disp[0]), float(Sim.disp[1]), float(Sim.disp[2])],
            path1=f"{Sim.dir_name}/parameters/\
forces.png",
        )
        s_bot, s_top = fun.create_surfaces(
            Sim.atom_info,
            s_depth=[Sim.box_size[0]/4, Sim.box_size[1]/4, Sim.box_size[2] - Sim.d_freeze],
            disp=[float(Sim.disp[0]), float(Sim.disp[1]), float(Sim.disp[2])],
        )
        #fr_coeff, fx, fy, fz = fun.get_friction(s_bot, atom_info)
        bottom=fun.create_bottom(np.array([
            [atom_info[:,:,2].min(), atom_info[:, :, 2].max()],
            [atom_info[:,:,3].min(), atom_info[:, :, 3].max()],
            [(fr_reg[0].box)[2,0],(fr_reg[0].box)[2,1]]],dtype=float),
            atom_info)
        
        fr_coeff, fx, fy, fz = fun.get_friction(bottom, atom_info)
        fun.save_friction_coeff(
            f"{Sim.dir_name}/parameters/\
friction.data",
            f"{Sim.dir_name}/parameters/\
{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.temp",
            timestep,
            fr_coeff,
            fy,
            fz,
        )
        fun.plot_friction_coeff(
            f"{Sim.dir_name}/parameters/\
friction.out",
            f"{Sim.dir_name}/parameters/\
friction.data",
            f"{Sim.dir_name}/parameters/\
forces_1.png",
            f"{Sim.dir_name}/parameters/\
friction.png",
            f"{Sim.dir_name}/parameters/\
fr_coeff.png"
        )
        fun.plot_parameters(
            f"{Sim.dir_name}/parameters/\
{Sim.matA}-{Sim.matB}_{Sim.disp[0]}x{Sim.disp[1]}x{Sim.disp[2]}.temp",
            f"{Sim.dir_name}/parameters/\
parameters.png",
            timestep,
        )
#        if Sim.MovieEnable:
#            fun.visualize1(
#                f"{Sim.dir_name}\
#.{Sim.MovieFormat}",
#                Sim.box_info,
#                Sim.atom_info,
#                Sim.mass,
#                n=5,
#                mode=Sim.MovieColormode,
#                angles=Sim.MovieAngles,
#                elev=Sim.MovieElev,
#            )
