#!/bin/bash

source /opt/HPC/init.sh
module load ScaLAPACK/2.1.0-gompi-2020a
/opt/lammps/mlip-2/bin/lmp_g++_openmpi  $1 $2
