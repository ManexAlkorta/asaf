import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as interp1d
import fun as fun

def reorder(matrix):
    mat1=np.zeros(np.shape(matrix))
    for i in range(0, len(matrix[0,:])):
        for j in range(0, len(matrix[0, :])):
            if matrix[0,i]<=mat1[0, j] or 0==mat1[0, j]:
                for k in range(len(mat1[0,:])-1, j, -1):
                    mat1[:, k]=mat1[:, k-1]
                mat1[:, j]=matrix[:, i]
                break
    return(mat1)

def SiCSiC(forces_z, forces_y, friction_norm, friction_men):
    f=plt.figure(figsize=(10, 8))
    f1=f.add_subplot(1,1,1)
    #forces_z=-1.60217733*10**(-9)*forces_z/(59.7*79.6)#Normal pressure in Bars.
    forces_z=-forces_z
    f1.scatter(forces_z, friction_norm, label=r"$\mu_f^n$")
    f1.scatter(forces_z, friction_mean, label=r"$\mu_f^m$")
    #f1.scatter(forces_z, forces_y, label=r"$F_y$(eV/$\AA$)")
    plt.xlim(0, 500)
    plt.ylim(0, 1)
    f1.set_xlabel(r"$F_z$(eV/$\AA$)", fontsize=20)
    f1.set_ylabel(r"Friction coefficient", fontsize=20)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    f.savefig("SiC-SiC.png")

def energy(path, path1, path2, ts):
    data = np.loadtxt(path, delimiter=" ", dtype=str)
    data = data[1:, :].astype(float)
    timestep=np.empty(len(data[:,1]))
    data1 = np.loadtxt(path1, delimiter=" ", dtype=str)
    data1 = data1[1:, :].astype(float)
    timestep1=np.empty(len(data1[:,1]))
    for i in range(0, len(data[:,1])):
        timestep[i]=ts*i*100
    for i in range(0, len(data1[:,1])):
        timestep1[i]=ts*i*100
    d=1
    data2=data
    data3=data1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data2[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data2[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data2[i, :]=np.average(data[i-d:i+d, :], axis=0)
    d=10
    for i in range(0, len(data1[:,0])):
        if (i-d<=0):
            data3[i, :]=np.average(data1[0:d, :], axis=0)
        elif (i+d>=len(data1[:,0])):
            data3[i, :]=np.average(data1[-d:, :], axis=0)
        else:
            data3[i, :]=np.average(data1[i-d:i+d, :], axis=0)
    xmax = timestep[-1]
    f = plt.figure(figsize=(4, 5))
    f1 = f.add_subplot(1, 1, 1)
    f1.set_xlabel(r"$t$ (ps)", fontsize=18)
    f1.set_ylabel(r"$E$(keV)", fontsize=18)
    f1.set_xlim(0, 15)
    f1.set_ylim(data3[0,1]+data3[0,0]-500, data3[0,1]+data3[0,0]+100)
    #plt.plot(timestep[:], data[:, 0], label=r"$E_k$")
    #plt.plot(timestep[:], data[:, 1], label=r"$E_p$")
    plt.plot(timestep[:], data2[:, 1]+data2[:,0]+(data3[0,1]+data3[0,0]-data2[0,1]-data2[0,0]), label=r"$E_T$")
    #plt.plot(timestep1[:], data1[:, 0], '--', color="tab:blue")
    #plt.plot(timestep1[:], data1[:, 1], '--', color="tab:orange")
    plt.plot(timestep1[:], data3[:, 1]+data3[:,0], color="tab:green")
    plt.xticks(fontsize=16)
    plt.yticks(ticks=[-166000, -165800], labels=[-116.0, -115.8], fontsize=16)
    plt.tight_layout()
    plt.savefig(path2, dpi=1200)


def vol_energy(path, path1, path2, path3, ts):
    data1 = np.loadtxt(path, delimiter="\n", dtype=str)
    data=np.empty([len(data1), 7],dtype=float)
    f=plt.figure(figsize=(10, 6))
    f1=f.add_subplot(1,1,1)
    f1.set_xlabel(r"$t$ (ps)", fontsize=18)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    
    for i in range (0, len(data1)):
        data[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep=np.empty(len(data[:,1]))
    for i in range(0, len(data[:, 0])):
        timestep[i]=data[i, 0]*ts
    data1 = np.loadtxt(path1, delimiter="\n", dtype=str)
    data2=np.empty([len(data1), 7],dtype=float)
    for i in range (0, len(data1)):
        data2[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep2=np.empty(len(data2[:,1]))
    for i in range(0, len(data2[:, 0])):
        timestep2[i]=data2[i, 0]*ts
    data1 = np.loadtxt(path2, delimiter="\n", dtype=str)
    data3=np.empty([len(data1), 7],dtype=float)
    for i in range (0, len(data1)):
        data3[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep3=np.empty(len(data3[:,1]))
    for i in range(0, len(data3[:, 0])):
        timestep3[i]=data3[i, 0]*ts
    f1.set_xlim(0, timestep[:].max())
    f1.set_ylabel(r"$T$(K)", fontsize=18)
    f2=f1.twinx()
    f2.set_ylabel(r"$V$(nm$^3$)", fontsize=18)
    f2.set_ylim(140, 210)
    f1.set_ylim(0, 700)
    f1.plot(timestep, data[:, 1], '--', color="tab:blue")
    f2.plot(timestep, data[:, 6]*10**(-3), color="tab:blue")
    f1.plot(timestep2, data2[:, 1], '--', color="tab:orange")
    f2.plot(timestep2, data2[:, 6]*10**(-3), color="tab:orange")
    f1.plot(timestep3, data3[:, 1], '--', color="tab:green")
    f2.plot(timestep3, data3[:, 6]*10**(-3), color="tab:green")
    f1.set_xlabel(r"$t$ (ps)", fontsize=18)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    plt.tight_layout()
    f.savefig(path3, dpi=1200)

def vol_energy1(path, path1, path2, path3, path4, path5, path6, ts, ts1):
    data1 = np.loadtxt(path, delimiter="\n", dtype=str)
    data=np.empty([len(data1), 7],dtype=float)
    f=plt.figure(figsize=(10, 6))
    f1=f.add_subplot(231)
    f1.set_ylim(0,350)
    yticks = f1.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)
    f4=f.add_subplot(232, sharey=f1)
    f7=f.add_subplot(233, sharey=f1)
    for i in range (0, len(data1)):
        data[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep=np.empty(len(data[:,1]))
    for i in range(0, len(data[:, 0])):
        timestep[i]=data[i, 0]*ts
    data1 = np.loadtxt(path1, delimiter="\n", dtype=str)
    data2=np.empty([len(data1), 7],dtype=float)
    for i in range (0, len(data1)):
        data2[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep2=np.empty(len(data2[:,1]))
    for i in range(0, len(data2[:, 0])):
        timestep2[i]=data2[i, 0]*ts
    data1 = np.loadtxt(path2, delimiter="\n", dtype=str)
    data3=np.empty([len(data1), 7],dtype=float)
    for i in range (0, len(data1)):
        data3[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep3=np.empty(len(data3[:,1]))
    for i in range(0, len(data3[:, 0])):
        timestep3[i]=data3[i, 0]*ts
    data1 = np.loadtxt(path4, delimiter="\n", dtype=str)
    data4=np.empty([len(data1), 7],dtype=float)
    for i in range (0, len(data1)):
        data4[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep4=np.empty(len(data4[:,1]))
    for i in range(0, len(data4[:, 0])):
        timestep4[i]=data4[i, 0]*ts1
    data1 = np.loadtxt(path5, delimiter="\n", dtype=str)
    data5=np.empty([len(data1), 7],dtype=float)
    for i in range (0, len(data1)):
        data5[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep5=np.empty(len(data5[:,1]))
    for i in range(0, len(data5[:, 0])):
        timestep5[i]=data5[i, 0]*ts1
    data1 = np.loadtxt(path6, delimiter="\n", dtype=str)
    data6=np.empty([len(data1), 7],dtype=float)
    for i in range (0, len(data1)):
        data6[i]=np.fromstring(str(data1[i]), sep=" ", dtype=float)
    timestep6=np.empty(len(data6[:,1]))
    for i in range(0, len(data6[:, 0])):
        timestep6[i]=data6[i, 0]*ts1
    f1.set_xlim(0, timestep[:].max())
    f1.set_ylabel(r"$T$(K)", fontsize=18)
    f3=f.add_subplot(234, sharex=f1)
    f3.set_ylim(175,230)
    f3.set_yticks(ticks=[180, 190, 200, 210, 220])
    f4.set_xlim(0, timestep2[:].max())
    f3.set_ylabel(r"$V$(nm$^3$)", fontsize=18)
    f6=f.add_subplot(235, sharex=f4)
    f6.set_ylim(175, 230)
    f7.set_xlim(0, timestep3[:].max())
    f6.set_yticks(ticks=[])
    f9=f.add_subplot(236, sharex=f7)
    f9.set_ylim(175, 230)
    f9.set_yticks(ticks=[])
    plt.setp(f1.get_xticklabels(), visible=False)
    plt.setp(f7.get_yticklabels(), visible=False)
    plt.setp(f7.get_xticklabels(), visible=False)
    yticks = f1.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)
    f1.plot(timestep, data[:, 1], color="lightcoral")
    f1.plot(timestep4, data4[:, 1], color="darkred")
    f10=f1.twinx()
    f10.set_yticks([])
    f10.plot(timestep, data[:, 4]*10**(-3), color="lightgreen")
    f10.plot(timestep4, data4[:, 4]*10**(-3), color="green")
    f3.plot(timestep, data[:, 6]*10**(-3), color="skyblue")
    f3.plot(timestep4, data4[:, 6]*10**(-3), color="darkblue")
    f2=f3.twinx()
    plt.setp(f2.get_yticklabels(), visible=False)
    f2.plot(timestep, data[:, 5]*10**(-5),color="violet")
    f2.plot(timestep4, data4[:, 5]*10**(-5),color="darkviolet")
    f4.plot(timestep2, data2[:, 1], color="lightcoral")
    f4.plot(timestep5, data5[:, 1], color="darkred")
    f11=f4.twinx()
    f11.set_yticks([])
    plt.setp(f4.get_yticklabels(), visible=False)
    f11.plot(timestep2, data2[:, 4]*10**(-3), color="lightgreen")
    f11.plot(timestep5, data5[:, 4]*10**(-3), color="green")
    f6.plot(timestep2, data2[:, 6]*10**(-3), color="skyblue")
    f6.plot(timestep5, data5[:, 6]*10**(-3), color="darkblue")
    f5=f6.twinx()
    f5.plot(timestep2, data2[:, 5]*10**(-5), color="violet")
    f5.plot(timestep5, data5[:, 5]*10**(-5), color="darkviolet")
    plt.setp(f5.get_yticklabels(), visible=False)
    f7.plot(timestep3, data3[:, 1], color="lightcoral")
    f7.plot(timestep6, data6[:, 1], color="darkred")
    f12=f7.twinx()
    plt.setp(f7.get_yticklabels(), visible=False)
    f12.plot(timestep3, data3[:, 4]*10**(-3), color="lightgreen")
    f12.plot(timestep6, data6[:, 4]*10**(-3), color="green")
    f9.plot(timestep3, data3[:, 6]*10**(-3), color="skyblue")
    f9.plot(timestep6, data6[:, 6]*10**(-3), color="darkblue")
    f8=f9.twinx()
    f8.plot(timestep3, data3[:, 5]*10**(-5), color="violet")
    f8.plot(timestep6, data6[:, 5]*10**(-5), color="darkviolet")
    #f3.set_xlabel(r"$t$ (ps)", fontsize=18)
    f6.set_xlabel(r"$t$ (ps)", fontsize=18)
    #f9.set_xlabel(r"$t$ (ps)", fontsize=18)
    f8.set_ylabel(r"$P$(Pa)", fontsize=18)
    f12.set_ylabel(r"$E$(keV)", fontsize=18)
    f2.set_ylim(-3, 0.5)
    f5.set_ylim(-3, 0.5)
    f8.set_ylim(-3, 0.5)
    f1.tick_params(labelsize=16)
    f3.tick_params(labelsize=16)
    f6.tick_params(labelsize=16)
    f9.tick_params(labelsize=16)
    f8.tick_params(labelsize=16)
    f12.tick_params(labelsize=16)
    #f12.set_yticks(ticks=[-69, -68.5, -68])
    xticks = f3.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f2.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f5.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f6.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f8.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f9.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    plt.tight_layout()
    plt.subplots_adjust(hspace=.0, wspace=.0)
    f.savefig(path3, dpi=1200)

def definitive1(path, path1, path2, path3, path4, path5, path6):
    data = np.loadtxt(path, delimiter="\t")
    data5 = np.loadtxt(path1, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data6=np.empty([len(data5[:, 0]), len(data5[0, :])])
    data[:, 4]=-data[:, 4]
    data5[:, 4]=-data5[:, 4]
    d=1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data5[:,0])):
        if (i-d<=0):
            data6[i, :]=np.average(data5[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data6[i, :]=np.average(data5[-d:, :], axis=0)
        else:
            data6[i, :]=np.average(data5[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    while abs(diff) < 0.05 * ke:
        try:
            diff = data[-1, 5] - data[-i, 5]
            i += 1
        except IndexError:
            i=len(data[:,5])-1
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data4=np.empty([3, len(data[:,0])])
    data7=reorder(np.array([data6[:, 4], data6[:, 1], data6[:, 3]]))
    data8=np.empty([3, len(data5[:,0])])
    data9=np.empty([3, len(data5[:,0])])
    d1=400
    d2=2
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data[:,0])):
        if (i-d2<=0):
            data4[:, i]=np.average(data2[:, 0:d2], axis=1)
        elif (i+d2>=len(data[:,0])):
            data4[:, i]=np.average(data2[:, -d2:], axis=1)
        else:
            data4[:, i]=np.average(data2[:, i-d2:i+d2+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d1<=0):
            data8[:, i]=np.average(data7[:, 0:d1], axis=1)
        elif (i+d1>=len(data5[:,0])):
            data8[:, i]=np.average(data7[:, -d1:], axis=1)
        else:
            data8[:, i]=np.average(data7[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d2<=0):
            data9[:, i]=np.average(data7[:, 0:d2], axis=1)
        elif (i+d2>=len(data5[:,0])):
            data9[:, i]=np.average(data7[:, -d2:], axis=1)
        else:
            data9[:, i]=np.average(data7[:, i-d2:i+d2+1], axis=1)
    f=plt.figure(figsize=(10, 6))
    f1=f.add_subplot(231)
    f1.set_ylim(0,350)
    yticks = f1.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)
    f4=f.add_subplot(232, sharey=f1)
    f7=f.add_subplot(233, sharey=f1)
    f1.set_ylabel(r"$T$(K)", fontsize=18)
    f3=f.add_subplot(234, sharex=f1)
    f3.set_ylim(175,230)
    f3.set_yticks(ticks=[180, 190, 200, 210, 220])
    f4.set_xlim(0, timestep2[:].max())
    f3.set_ylabel(r"$V$(nm$^3$)", fontsize=18)
    f6=f.add_subplot(235, sharex=f4)
    f6.set_ylim(175, 230)
    f7.set_xlim(0, timestep3[:].max())
    f6.set_yticks(ticks=[])
    f9=f.add_subplot(236, sharex=f7)
    f9.set_ylim(175, 230)
    f9.set_yticks(ticks=[])
    plt.setp(f1.get_xticklabels(), visible=False)
    plt.setp(f7.get_yticklabels(), visible=False)
    plt.setp(f7.get_xticklabels(), visible=False)
    yticks = f1.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)
    f1.plot(data2[0, :-20], data3[2, :-20], color="darkred")
    ############################################
    ################ EDITATU ###################
    f1.plot(timestep4, data4[:, 1], color="darkred")
    f10=f1.twinx()
    f10.set_yticks([])
    f10.plot(timestep, data[:, 4]*10**(-3), color="lightgreen")
    f10.plot(timestep4, data4[:, 4]*10**(-3), color="green")
    f3.plot(timestep, data[:, 6]*10**(-3), color="skyblue")
    f3.plot(timestep4, data4[:, 6]*10**(-3), color="darkblue")
    f2=f3.twinx()
    plt.setp(f2.get_yticklabels(), visible=False)
    f2.plot(timestep, data[:, 5]*10**(-5),color="violet")
    f2.plot(timestep4, data4[:, 5]*10**(-5),color="darkviolet")
    f4.plot(timestep2, data2[:, 1], color="lightcoral")
    f4.plot(timestep5, data5[:, 1], color="darkred")
    f11=f4.twinx()
    f11.set_yticks([])
    plt.setp(f4.get_yticklabels(), visible=False)
    f11.plot(timestep2, data2[:, 4]*10**(-3), color="lightgreen")
    f11.plot(timestep5, data5[:, 4]*10**(-3), color="green")
    f6.plot(timestep2, data2[:, 6]*10**(-3), color="skyblue")
    f6.plot(timestep5, data5[:, 6]*10**(-3), color="darkblue")
    f5=f6.twinx()
    f5.plot(timestep2, data2[:, 5]*10**(-5), color="violet")
    f5.plot(timestep5, data5[:, 5]*10**(-5), color="darkviolet")
    plt.setp(f5.get_yticklabels(), visible=False)
    f7.plot(timestep3, data3[:, 1], color="lightcoral")
    f7.plot(timestep6, data6[:, 1], color="darkred")
    f12=f7.twinx()
    plt.setp(f7.get_yticklabels(), visible=False)
    f12.plot(timestep3, data3[:, 4]*10**(-3), color="lightgreen")
    f12.plot(timestep6, data6[:, 4]*10**(-3), color="green")
    f9.plot(timestep3, data3[:, 6]*10**(-3), color="skyblue")
    f9.plot(timestep6, data6[:, 6]*10**(-3), color="darkblue")
    f8=f9.twinx()
    f8.plot(timestep3, data3[:, 5]*10**(-5), color="violet")
    f8.plot(timestep6, data6[:, 5]*10**(-5), color="darkviolet")
    #f3.set_xlabel(r"$t$ (ps)", fontsize=18)
    f6.set_xlabel(r"$t$ (ps)", fontsize=18)
    #f9.set_xlabel(r"$t$ (ps)", fontsize=18)
    f8.set_ylabel(r"$P$(Pa)", fontsize=18)
    f12.set_ylabel(r"$E$(keV)", fontsize=18)
    f2.set_ylim(-3, 0.5)
    f5.set_ylim(-3, 0.5)
    f8.set_ylim(-3, 0.5)
    f1.tick_params(labelsize=16)
    f3.tick_params(labelsize=16)
    f6.tick_params(labelsize=16)
    f9.tick_params(labelsize=16)
    f8.tick_params(labelsize=16)
    f12.tick_params(labelsize=16)
    #f12.set_yticks(ticks=[-69, -68.5, -68])
    xticks = f3.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f2.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f5.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f6.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f8.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f9.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    plt.tight_layout()
    plt.subplots_adjust(hspace=.0, wspace=.0)
    f.savefig(path3, dpi=1200)
    

def RDF_dif(path, path1, path2, path3, path4, image_name):
    timestep, n_atoms, n_frames, box_info, atom_info, r, r0 = fun.read_dump(
        path, ":"
    )
    f=plt.figure(figsize=(10, 8))
    R_cut, n = 5, 100
    f1=f.add_subplot(1,1,1)
    x = np.arange(0, R_cut, R_cut / n)
    frame=50
    RDF = fun.get_RDF(n_atoms, n_frames, frame, atom_info, r, R_cut, n)
    f1.plot(x, RDF, label=r"nvt")
    timestep, n_atoms, n_frames, box_info, atom_info, r, r0 = fun.read_dump(
        path1, ":"
    )
    frame=50
    RDF = fun.get_RDF(n_atoms, n_frames, frame, atom_info, r, R_cut, n)
    f1.plot(x, RDF, label=r"npt")
    timestep, n_atoms, n_frames, box_info, atom_info, r, r0 = fun.read_dump(
        path2, ":"
    )
    frame=50
    RDF = fun.get_RDF(n_atoms, n_frames, frame, atom_info, r, R_cut, n)
    f1.plot(x, RDF, '--', label=r"nvt")
    timestep, n_atoms, n_frames, box_info, atom_info, r, r0 = fun.read_dump(
        path3, ":"
    )
    frame=50
    RDF = fun.get_RDF(n_atoms, n_frames, frame, atom_info, r, R_cut, n)
    f1.plot(x, RDF, '--', label=r"npt")
    #timestep, n_atoms, n_frames, box_info, atom_info, r, r0 = fun.read_cfg(
    #    path4, ":"
    #)
    #frame=500
    #RDF = fun.get_RDF(n_atoms, n_frames, frame, atom_info, r, R_cut, n)
    #f1.plot(x, RDF, '--',label=r"ab-initio")
    f1.set_xlabel(r"r($\AA$)", fontsize=18)
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    f.savefig(image_name, dpi=1200)

def force_dist_1(path, path1, path2, image_name):
    data = np.loadtxt(path, delimiter="\t")
    data5 = np.loadtxt(path1, delimiter="\t")
    data10 = np.loadtxt(path2, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data6=np.empty([len(data5[:, 0]), len(data5[0, :])])
    data11=np.empty([len(data10[:, 0]), len(data10[0, :])])
    data[:, 4]=-data[:, 4]
    data5[:, 4]=-data5[:, 4]
    data10[:, 4]=-data10[:, 4]
    d=1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data5[:,0])):
        if (i-d<=0):
            data6[i, :]=np.average(data5[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data6[i, :]=np.average(data5[-d:, :], axis=0)
        else:
            data6[i, :]=np.average(data5[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data10[:,0])):
        if (i-d<=0):
            data11[i, :]=np.average(data10[0:d, :], axis=0)
        elif (i+d>=len(data10[:,0])):
            data11[i, :]=np.average(data10[-d:, :], axis=0)
        else:
            data11[i, :]=np.average(data10[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    while abs(diff) < 0.05 * ke:
        try:
            diff = data[-1, 5] - data[-i, 5]
            i += 1
        except IndexError:
            i=len(data[:,5])-1
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data4=np.empty([3, len(data[:,0])])
    data7=reorder(np.array([data6[:, 4], data6[:, 1], data6[:, 3]]))
    data8=np.empty([3, len(data5[:,0])])
    data9=np.empty([3, len(data5[:,0])])
    data12=reorder(np.array([data11[:, 4], data11[:, 1], data11[:, 3]]))
    data13=np.empty([3, len(data10[:,0])])
    data14=np.empty([3, len(data10[:,0])])
    d1=30
    d2=2
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data[:,0])):
        if (i-d2<=0):
            data4[:, i]=np.average(data2[:, 0:d2], axis=1)
        elif (i+d2>=len(data[:,0])):
            data4[:, i]=np.average(data2[:, -d2:], axis=1)
        else:
            data4[:, i]=np.average(data2[:, i-d2:i+d2+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d1<=0):
            data8[:, i]=np.average(data7[:, 0:d1], axis=1)
        elif (i+d1>=len(data5[:,0])):
            data8[:, i]=np.average(data7[:, -d1:], axis=1)
        else:
            data8[:, i]=np.average(data7[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d2<=0):
            data9[:, i]=np.average(data7[:, 0:d2], axis=1)
        elif (i+d2>=len(data5[:,0])):
            data9[:, i]=np.average(data7[:, -d2:], axis=1)
        else:
            data9[:, i]=np.average(data7[:, i-d2:i+d2+1], axis=1)
    for i in range(0, len(data10[:,0])):
        if (i-d1<=0):
            data13[:, i]=np.average(data12[:, 0:d1], axis=1)
        elif (i+d1>=len(data10[:,0])):
            data13[:, i]=np.average(data12[:, -d1:], axis=1)
        else:
            data13[:, i]=np.average(data12[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data10[:,0])):
        if (i-d2<=0):
            data14[:, i]=np.average(data12[:, 0:d2], axis=1)
        elif (i+d2>=len(data10[:,0])):
            data14[:, i]=np.average(data12[:, -d2:], axis=1)
        else:
            data14[:, i]=np.average(data12[:, i-d2:i+d2+1], axis=1)
    f = plt.figure(figsize=(10, 5))
    f1 = f.add_subplot(111)
    f2 = f1.twinx()
    #f3.set_ylim(0,1)
    plt.setp(f2.get_xticklabels(), visible=False)
    plt.setp(f2.get_yticklabels(), visible=False)
    #f1.plot(data2[0, :], data4[1, :], color="lightblue")
    #f1.plot(data2[0, :-20], data3[1, :-20], color="darkblue")
    #f1.plot(data7[0, :-20], data8[1, :-20], color="darkviolet")
    #f1.set_ylim(0, data[-1, 2])
    #f2.plot(data2[0, :], data4[2, :], color="lightcoral")
    f1.plot(data2[0, :-20], data3[2, :-20], color="darkred")
    f1.plot(data7[0, :-20], data8[2, :-20], color="green")
    f1.plot(data12[0, :-20], data13[2, :-20], color="darkviolet")
    xmax=500
    plt.xlim(0, xmax)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$\mu_f$", fontsize=18)
    f1.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$f_{\parallel}$(eV/$\AA$)", fontsize=18)
    #dist, x=get_force_distr(400, 10, data3)
    xnew=np.arange(0, xmax, 1)
    dist, x, patches=plt.hist(data[:, 4], bins=10, alpha=0)
    try:
        distnew=interp1d.interp1d(x[:-1], dist, kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="darkred")
        f2.plot(xnew, distnew(xnew), '--', color="darkred")
    except ValueError:
        x1=np.arange(x[-1]+1, 500, 1)
        dist1=np.zeros([int(500-x[-1])])
        distnew=interp1d.interp1d(np.append(x[:-1],x1), np.append(dist,dist1), kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="darkred")
        f2.plot(xnew, distnew(xnew), '--', color="darkred")
    dist, x, patches=plt.hist(data5[:, 4], bins=10, alpha=0)
    try:
        distnew=interp1d.interp1d(x[:-1], dist, kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="green")
        f2.plot(xnew, distnew(xnew), '--', color="green")
    except ValueError:
        x1=np.arange(x[-1]+1, 500, 1)
        dist1=np.zeros([int(500-x[-1])])
        distnew=interp1d.interp1d(np.append(x[:-1],x1), np.append(dist,dist1), kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="green")
        f2.plot(xnew, distnew(xnew), '--', color="green")
    f2.set_yticks([])
    f2.set_ylim(0,150)
    plt.tight_layout()
    plt.savefig(image_name)

def force_dist(path, path1, image_name):
    data = np.loadtxt(path, delimiter="\t")
    data5 = np.loadtxt(path1, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data6=np.empty([len(data5[:, 0]), len(data5[0, :])])
    data[:, 4]=-data[:, 4]
    data5[:, 4]=-data5[:, 4]
    d=1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data5[:,0])):
        if (i-d<=0):
            data6[i, :]=np.average(data5[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data6[i, :]=np.average(data5[-d:, :], axis=0)
        else:
            data6[i, :]=np.average(data5[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    while abs(diff) < 0.05 * ke:
        try:
            diff = data[-1, 5] - data[-i, 5]
            i += 1
        except IndexError:
            i=len(data[:,5])-1
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data4=np.empty([3, len(data[:,0])])
    data7=reorder(np.array([data6[:, 4], data6[:, 1], data6[:, 3]]))
    data8=np.empty([3, len(data5[:,0])])
    data9=np.empty([3, len(data5[:,0])])
    d1=200
    d2=2
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data[:,0])):
        if (i-d2<=0):
            data4[:, i]=np.average(data2[:, 0:d2], axis=1)
        elif (i+d2>=len(data[:,0])):
            data4[:, i]=np.average(data2[:, -d2:], axis=1)
        else:
            data4[:, i]=np.average(data2[:, i-d2:i+d2+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d1<=0):
            data8[:, i]=np.average(data7[:, 0:d1], axis=1)
        elif (i+d1>=len(data5[:,0])):
            data8[:, i]=np.average(data7[:, -d1:], axis=1)
        else:
            data8[:, i]=np.average(data7[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d2<=0):
            data9[:, i]=np.average(data7[:, 0:d2], axis=1)
        elif (i+d2>=len(data5[:,0])):
            data9[:, i]=np.average(data7[:, -d2:], axis=1)
        else:
            data9[:, i]=np.average(data7[:, i-d2:i+d2+1], axis=1)
    f = plt.figure(figsize=(10, 5))
    f1 = f.add_subplot(111)
    f2 = f1.twinx()
    #f3.set_ylim(0,1)
    plt.setp(f2.get_xticklabels(), visible=False)
    plt.setp(f2.get_yticklabels(), visible=False)
    #f1.plot(data2[0, :], data4[1, :], color="lightblue")
    #f1.plot(data2[0, :-20], data3[1, :-20], color="darkblue")
    #f1.plot(data7[0, :-20], data8[1, :-20], color="darkviolet")
    #f1.set_ylim(0, data[-1, 2])
    #f2.plot(data2[0, :], data4[2, :], color="lightcoral")
    f1.plot(data2[0, :-20], data3[2, :-20], color="darkred")
    f1.plot(data7[0, :-20], data8[2, :-20], color="green")
    xmax=500
    plt.xlim(0, xmax)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$\mu_f$", fontsize=18)
    f1.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$f_{\parallel}$(eV/$\AA$)", fontsize=18)
    xnew=np.arange(0, xmax, 1)
    dist, x, patches=plt.hist(data[:, 4], bins=10, alpha=0)
    try:
        distnew=interp1d.interp1d(x[:-1], dist, kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="darkred")
        f2.plot(xnew, distnew(xnew), '--', color="darkred")
    except ValueError:
        x1=np.arange(x[-1]+1, 500, 1)
        dist1=np.zeros([int(500-x[-1])])
        distnew=interp1d.interp1d(np.append(x[:-1],x1), np.append(dist,dist1), kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="darkred")
        f2.plot(xnew, distnew(xnew), '--', color="darkred")
    dist, x, patches=plt.hist(data5[:, 4], bins=10, alpha=0)
    try:
        distnew=interp1d.interp1d(x[:-1], dist, kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="green")
        f2.plot(xnew, distnew(xnew), '--', color="green")
    except ValueError:
        x1=np.arange(x[-1]+1, 500, 1)
        dist1=np.zeros([int(500-x[-1])])
        distnew=interp1d.interp1d(np.append(x[:-1],x1), np.append(dist,dist1), kind='cubic')
        f2.fill_between(xnew, distnew(xnew), step="pre", alpha=0.1, color="green")
        f2.plot(xnew, distnew(xnew), '--', color="green")
    f2.set_yticks([])
    f2.set_ylim(0,150)
    plt.tight_layout()
    plt.savefig(image_name)

def definitive(path, path1, image_name):
    data = np.loadtxt(path, delimiter="\t")
    data5 = np.loadtxt(path1, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data6=np.empty([len(data5[:, 0]), len(data5[0, :])])
    data[:, 4]=-data[:, 4]
    data5[:, 4]=-data5[:, 4]
    d=1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data5[:,0])):
        if (i-d<=0):
            data6[i, :]=np.average(data5[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data6[i, :]=np.average(data5[-d:, :], axis=0)
        else:
            data6[i, :]=np.average(data5[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    while abs(diff) < 0.05 * ke:
        try:
            diff = data[-1, 5] - data[-i, 5]
            i += 1
        except IndexError:
            i=len(data[:,5])-1
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data4=np.empty([3, len(data[:,0])])
    data7=reorder(np.array([data6[:, 4], data6[:, 1], data6[:, 3]]))
    data8=np.empty([3, len(data5[:,0])])
    data9=np.empty([3, len(data5[:,0])])
    d1=400
    d2=2
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data[:,0])):
        if (i-d2<=0):
            data4[:, i]=np.average(data2[:, 0:d2], axis=1)
        elif (i+d2>=len(data[:,0])):
            data4[:, i]=np.average(data2[:, -d2:], axis=1)
        else:
            data4[:, i]=np.average(data2[:, i-d2:i+d2+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d1<=0):
            data8[:, i]=np.average(data7[:, 0:d1], axis=1)
        elif (i+d1>=len(data5[:,0])):
            data8[:, i]=np.average(data7[:, -d1:], axis=1)
        else:
            data8[:, i]=np.average(data7[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d2<=0):
            data9[:, i]=np.average(data7[:, 0:d2], axis=1)
        elif (i+d2>=len(data5[:,0])):
            data9[:, i]=np.average(data7[:, -d2:], axis=1)
        else:
            data9[:, i]=np.average(data7[:, i-d2:i+d2+1], axis=1)
    f = plt.figure(figsize=(5, 6))
    f1 = f.add_subplot(111)
    f2 = f1.twinx()
    #f3.set_ylim(0,1)
    plt.setp(f2.get_xticklabels(), visible=False)
    #f1.plot(data2[0, :], data4[1, :], color="lightblue")
    #f1.plot(data2[0, :-20], data3[1, :-20], color="darkblue")
    #f1.plot(data7[0, :-20], data8[1, :-20], color="darkviolet")
    #f1.set_ylim(0, data[-1, 2])
    #f2.plot(data2[0, :], data4[2, :], color="lightcoral")
    f1.plot(data2[0, :-20], data3[2, :-20], color="darkred")
    f2.plot(data2[0, :-20], data3[1, :-20], color="lightcoral")
    f1.plot(data7[0, :-20], data8[2, :-20], color="green")
    f2.plot(data7[0, :-20], data8[1, :-20], color="lightgreen")
    xmax=500
    plt.xlim(0, xmax)
    f1.tick_params(labelsize=16)
    f2.tick_params(labelsize=16)
    f2.set_ylabel(r"$\mu_f$", fontsize=18)
    f1.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$f_{\parallel}$(eV/$\AA$)", fontsize=18)
    f1.set_ylim([0,100])
    f1.set_xlim([0, 400])
    f2.set_ylim(0,2)
    plt.tight_layout()
    plt.savefig(image_name)

def definitive_vel(path, path1, path2, path3, path4, path5, image_name):
    data = np.loadtxt(path, delimiter="\t")
    data5 = np.loadtxt(path1, delimiter="\t")
    data10 = np.loadtxt(path2, delimiter="\t")
    data15 = np.loadtxt(path3, delimiter="\t")
    data20 = np.loadtxt(path4, delimiter="\t")
    data25 = np.loadtxt(path5, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data6=np.empty([len(data5[:, 0]), len(data5[0, :])])
    data11=np.empty([len(data10[:, 0]), len(data10[0, :])])
    data16=np.empty([len(data15[:, 0]), len(data15[0, :])])
    data21=np.empty([len(data20[:, 0]), len(data20[0, :])])
    data26=np.empty([len(data25[:, 0]), len(data25[0, :])])
    data[:, 4]=-data[:, 4]
    data5[:, 4]=-data5[:, 4]
    data10[:, 4]=-data10[:, 4]
    data15[:, 4]=-data15[:, 4]
    data20[:, 4]=-data20[:, 4]
    data25[:, 4]=-data25[:, 4]
    d=1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data5[:,0])):
        if (i-d<=0):
            data6[i, :]=np.average(data5[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data6[i, :]=np.average(data5[-d:, :], axis=0)
        else:
            data6[i, :]=np.average(data5[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data10[:,0])):
        if (i-d<=0):
            data11[i, :]=np.average(data10[0:d, :], axis=0)
        elif (i+d>=len(data10[:,0])):
            data11[i, :]=np.average(data10[-d:, :], axis=0)
        else:
            data11[i, :]=np.average(data10[i-d:i+d+1, :], axis=0)
    for i in range(0, len(data15[:,0])):
        if (i-d<=0):
            data16[i, :]=np.average(data15[0:d, :], axis=0)
        elif (i+d>=len(data15[:,0])):
            data16[i, :]=np.average(data15[-d:, :], axis=0)
        else:
            data16[i, :]=np.average(data15[i-d:i+d+1, :], axis=0)
    for i in range(0, len(data20[:,0])):
        if (i-d<=0):
            data21[i, :]=np.average(data20[0:d, :], axis=0)
        elif (i+d>=len(data20[:,0])):
            data21[i, :]=np.average(data20[-d:, :], axis=0)
        else:
            data21[i, :]=np.average(data20[i-d:i+d+1, :], axis=0)
    for i in range(0, len(data25[:,0])):
        if (i-d<=0):
            data26[i, :]=np.average(data25[0:d, :], axis=0)
        elif (i+d>=len(data25[:,0])):
            data26[i, :]=np.average(data25[-d:, :], axis=0)
        else:
            data26[i, :]=np.average(data25[i-d:i+d+1, :], axis=0)
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data7=reorder(np.array([data6[:, 4], data6[:, 1], data6[:, 3]]))
    data8=np.empty([3, len(data5[:,0])])
    data12=reorder(np.array([data11[:, 4], data11[:, 1], data11[:, 3]]))
    data13=np.empty([3, len(data10[:,0])])
    data17=reorder(np.array([data16[:, 4], data16[:, 1], data16[:, 3]]))
    data18=np.empty([3, len(data15[:,0])])
    data22=reorder(np.array([data21[:, 4], data21[:, 1], data21[:, 3]]))
    data23=np.empty([3, len(data20[:,0])])
    data27=reorder(np.array([data26[:, 4], data26[:, 1], data26[:, 3]]))
    data28=np.empty([3, len(data25[:,0])])
    d1=400
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d1<=0):
            data8[:, i]=np.average(data7[:, 0:d1], axis=1)
        elif (i+d1>=len(data5[:,0])):
            data8[:, i]=np.average(data7[:, -d1:], axis=1)
        else:
            data8[:, i]=np.average(data7[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data10[:,0])):
        if (i-d1<=0):
            data13[:, i]=np.average(data12[:, 0:d1], axis=1)
        elif (i+d1>=len(data10[:,0])):
            data13[:, i]=np.average(data12[:, -d1:], axis=1)
        else:
            data13[:, i]=np.average(data12[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data15[:,0])):
        if (i-d1<=0):
            data18[:, i]=np.average(data17[:, 0:d1], axis=1)
        elif (i+d1>=len(data10[:,0])):
            data18[:, i]=np.average(data17[:, -d1:], axis=1)
        else:
            data18[:, i]=np.average(data17[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data20[:,0])):
        if (i-d1<=0):
            data23[:, i]=np.average(data22[:, 0:d1], axis=1)
        elif (i+d1>=len(data20[:,0])):
            data23[:, i]=np.average(data22[:, -d1:], axis=1)
        else:
            data23[:, i]=np.average(data22[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data25[:,0])):
        if (i-d1<=0):
            data28[:, i]=np.average(data27[:, 0:d1], axis=1)
        elif (i+d1>=len(data25[:,0])):
            data28[:, i]=np.average(data27[:, -d1:], axis=1)
        else:
            data28[:, i]=np.average(data27[:, i-d1:i+d1+1], axis=1)
    f = plt.figure(figsize=(10, 6))
    f1 = f.add_subplot(121)
    f2 = f1.twinx()
    plt.setp(f2.get_xticklabels(), visible=False)
    plt.setp(f2.get_yticklabels(), visible=False)
    f1.plot(data3[0, :-20], data3[2, :-20], color="darkred")
    f2.plot(data3[0, :-20], data3[2, :-20]/data3[0, :-20], color="lightcoral")
    f1.plot(data8[0, :-20], data8[2, :-20], color="green")
    f2.plot(data8[0, :-20], data8[2, :-20]/data8[0, :-20], color="lightgreen")
    f1.plot(data13[0, :-20], data13[2, :-20], color="darkviolet")
    f2.plot(data13[0, :-20], data13[2, :-20]/data13[0, :-20], color="violet")
    xmax=500
    plt.xlim(0, xmax)
    f1.tick_params(labelsize=16)
    f2.tick_params(labelsize=16)
    f2.set_ylabel(r"$\mu_f$", fontsize=18)
    f1.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$f_{\parallel}$(eV/$\AA$)", fontsize=18)
    f1.set_ylim([0,100])
    f1.set_xlim([0, 400])
    f2.set_ylim(0,2)
    f3 = f.add_subplot(122)
    f4 = f3.twinx()
    plt.setp(f3.get_yticklabels(), visible=False)
    plt.setp(f4.get_xticklabels(), visible=False)
    f3.plot(data18[0, :-20], data18[2, :-20], color="darkred", label=r"$v_s=100$ m/s")
    f4.plot(data18[0, :-20], data18[2, :-20]/data18[0, :-20], color="lightcoral")
    f3.plot(data23[0, :-20], data23[2, :-20], color="green", label=r"$v_s=50$ m/s")
    f4.plot(data23[0, :-20], data23[2, :-20]/data23[0, :-20], color="lightgreen")
    f3.plot(data28[0, :-20], data28[2, :-20], color="darkviolet", label=r"$v_s=10$ m/s")
    f4.plot(data28[0, :-20], data28[2, :-20]/data28[0, :-20], color="violet")
    xmax=500
    plt.xlim(0, xmax)
    f3.tick_params(labelsize=16)
    f4.tick_params(labelsize=16)
    f4.set_ylabel(r"$\mu_f$", fontsize=18)
    f3.tick_params(labelsize=16)
    f3.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f3.set_ylim([0,100])
    f3.set_xlim([0, 400])
    f4.set_ylim(0,2)
    f3.legend(fontsize=16)
    xticks = f1.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f3.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    plt.tight_layout()
    plt.subplots_adjust(wspace=.0)
    plt.savefig(image_name)

def definitive_temp(path, path1, path2, path3, path4, path5, image_name):
    data = np.loadtxt(path, delimiter="\t")
    data5 = np.loadtxt(path1, delimiter="\t")
    data10 = np.loadtxt(path2, delimiter="\t")
    data15 = np.loadtxt(path3, delimiter="\t")
    data20 = np.loadtxt(path4, delimiter="\t")
    data25 = np.loadtxt(path5, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data6=np.empty([len(data5[:, 0]), len(data5[0, :])])
    data11=np.empty([len(data10[:, 0]), len(data10[0, :])])
    data16=np.empty([len(data15[:, 0]), len(data15[0, :])])
    data21=np.empty([len(data20[:, 0]), len(data20[0, :])])
    data26=np.empty([len(data25[:, 0]), len(data25[0, :])])
    data[:, 4]=-data[:, 4]
    data5[:, 4]=-data5[:, 4]
    data10[:, 4]=-data10[:, 4]
    data15[:, 4]=-data15[:, 4]
    data20[:, 4]=-data20[:, 4]
    data25[:, 4]=-data25[:, 4]
    d=1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data5[:,0])):
        if (i-d<=0):
            data6[i, :]=np.average(data5[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data6[i, :]=np.average(data5[-d:, :], axis=0)
        else:
            data6[i, :]=np.average(data5[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data10[:,0])):
        if (i-d<=0):
            data11[i, :]=np.average(data10[0:d, :], axis=0)
        elif (i+d>=len(data10[:,0])):
            data11[i, :]=np.average(data10[-d:, :], axis=0)
        else:
            data11[i, :]=np.average(data10[i-d:i+d+1, :], axis=0)
    for i in range(0, len(data15[:,0])):
        if (i-d<=0):
            data16[i, :]=np.average(data15[0:d, :], axis=0)
        elif (i+d>=len(data15[:,0])):
            data16[i, :]=np.average(data15[-d:, :], axis=0)
        else:
            data16[i, :]=np.average(data15[i-d:i+d+1, :], axis=0)
    for i in range(0, len(data20[:,0])):
        if (i-d<=0):
            data21[i, :]=np.average(data20[0:d, :], axis=0)
        elif (i+d>=len(data20[:,0])):
            data21[i, :]=np.average(data20[-d:, :], axis=0)
        else:
            data21[i, :]=np.average(data20[i-d:i+d+1, :], axis=0)
    for i in range(0, len(data25[:,0])):
        if (i-d<=0):
            data26[i, :]=np.average(data25[0:d, :], axis=0)
        elif (i+d>=len(data25[:,0])):
            data26[i, :]=np.average(data25[-d:, :], axis=0)
        else:
            data26[i, :]=np.average(data25[i-d:i+d+1, :], axis=0)
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data7=reorder(np.array([data6[:, 4], data6[:, 1], data6[:, 3]]))
    data8=np.empty([3, len(data5[:,0])])
    data12=reorder(np.array([data11[:, 4], data11[:, 1], data11[:, 3]]))
    data13=np.empty([3, len(data10[:,0])])
    data17=reorder(np.array([data16[:, 4], data16[:, 1], data16[:, 3]]))
    data18=np.empty([3, len(data15[:,0])])
    data22=reorder(np.array([data21[:, 4], data21[:, 1], data21[:, 3]]))
    data23=np.empty([3, len(data20[:,0])])
    data27=reorder(np.array([data26[:, 4], data26[:, 1], data26[:, 3]]))
    data28=np.empty([3, len(data25[:,0])])
    d1=400
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d1<=0):
            data8[:, i]=np.average(data7[:, 0:d1], axis=1)
        elif (i+d1>=len(data5[:,0])):
            data8[:, i]=np.average(data7[:, -d1:], axis=1)
        else:
            data8[:, i]=np.average(data7[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data10[:,0])):
        if (i-d1<=0):
            data13[:, i]=np.average(data12[:, 0:d1], axis=1)
        elif (i+d1>=len(data10[:,0])):
            data13[:, i]=np.average(data12[:, -d1:], axis=1)
        else:
            data13[:, i]=np.average(data12[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data15[:,0])):
        if (i-d1<=0):
            data18[:, i]=np.average(data17[:, 0:d1], axis=1)
        elif (i+d1>=len(data10[:,0])):
            data18[:, i]=np.average(data17[:, -d1:], axis=1)
        else:
            data18[:, i]=np.average(data17[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data20[:,0])):
        if (i-d1<=0):
            data23[:, i]=np.average(data22[:, 0:d1], axis=1)
        elif (i+d1>=len(data20[:,0])):
            data23[:, i]=np.average(data22[:, -d1:], axis=1)
        else:
            data23[:, i]=np.average(data22[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data25[:,0])):
        if (i-d1<=0):
            data28[:, i]=np.average(data27[:, 0:d1], axis=1)
        elif (i+d1>=len(data25[:,0])):
            data28[:, i]=np.average(data27[:, -d1:], axis=1)
        else:
            data28[:, i]=np.average(data27[:, i-d1:i+d1+1], axis=1)
    f = plt.figure(figsize=(10, 6))
    f1 = f.add_subplot(131)
    f2 = f1.twinx()
    plt.setp(f2.get_xticklabels(), visible=False)
    plt.setp(f2.get_yticklabels(), visible=False)
    f1.plot(data3[0, :-20], data3[2, :-20], color="darkred")
    f2.plot(data3[0, :-20], data3[2, :-20]/data3[0, :-20], color="lightcoral")
    f1.plot(data8[0, :-20], data8[2, :-20], '--', color="darkred")
    f2.plot(data8[0, :-20], data8[2, :-20]/data8[0, :-20], '--', color="lightcoral")
    xmax=500
    plt.xlim(0, xmax)
    f1.tick_params(labelsize=16)
    f2.tick_params(labelsize=16)
    f2.set_ylabel(r"$\mu_f$", fontsize=18)
    f1.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$f_{\parallel}$(eV/$\AA$)", fontsize=18)
    f1.set_ylim([0,100])
    f1.set_xlim([0, 400])
    f2.set_ylim(0,2)
    f3 = f.add_subplot(132)
    f4 = f3.twinx()
    plt.setp(f3.get_yticklabels(), visible=False)
    plt.setp(f4.get_yticklabels(), visible=False)
    plt.setp(f4.get_xticklabels(), visible=False)
    f3.plot(data13[0, :-20], data13[2, :-20], color="green")
    f4.plot(data13[0, :-20], data13[2, :-20]/data13[0, :-20], color="lightgreen")
    f3.plot(data18[0, :-20], data18[2, :-20], '--', color="green")
    f4.plot(data18[0, :-20], data18[2, :-20]/data18[0, :-20], '--', color="lightgreen")
    xmax=500
    plt.xlim(0, xmax)
    f3.tick_params(labelsize=16)
    f4.tick_params(labelsize=16)
    f3.tick_params(labelsize=16)
    f3.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f3.set_ylim([0,100])
    f3.set_xlim([0, 400])
    f4.set_ylim(0,2)
    f5 = f.add_subplot(133)
    f6 = f5.twinx()
    plt.setp(f5.get_yticklabels(), visible=False)
    plt.setp(f6.get_xticklabels(), visible=False)
    f5.plot(data23[0, :-20], data23[2, :-20], color="darkviolet")
    f6.plot(data23[0, :-20], data23[2, :-20]/data23[0, :-20], color="violet")
    f5.plot(data28[0, :-20], data28[2, :-20], '--', color="darkviolet")
    f6.plot(data28[0, :-20], data28[2, :-20]/data28[0, :-20], '--', color="violet")
    xmax=500
    plt.xlim(0, xmax)
    f5.tick_params(labelsize=16)
    f6.tick_params(labelsize=16)
    f6.set_ylabel(r"$\mu_f$", fontsize=18)
    f5.tick_params(labelsize=16)
    f5.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f5.set_ylim([0,100])
    f5.set_xlim([0, 400])
    f6.set_ylim(0,2)
    xticks = f1.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f3.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    xticks = f5.xaxis.get_major_ticks()
    xticks[0].label1.set_visible(False)
    xticks[-1].label1.set_visible(False)
    plt.tight_layout()
    plt.subplots_adjust(wspace=.0)
    plt.savefig(image_name)

def force_dist(path, path1, image_name):
    data = np.loadtxt(path, delimiter="\t")
    data5 = np.loadtxt(path1, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data6=np.empty([len(data5[:, 0]), len(data5[0, :])])
    data[:, 4]=-data[:, 4]
    data5[:, 4]=-data5[:, 4]
    d=1
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    for i in range(0, len(data5[:,0])):
        if (i-d<=0):
            data6[i, :]=np.average(data5[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data6[i, :]=np.average(data5[-d:, :], axis=0)
        else:
            data6[i, :]=np.average(data5[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    while abs(diff) < 0.05 * ke:
        try:
            diff = data[-1, 5] - data[-i, 5]
            i += 1
        except IndexError:
            i=len(data[:,5])-1
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data4=np.empty([3, len(data[:,0])])
    data7=reorder(np.array([data6[:, 4], data6[:, 1], data6[:, 3]]))
    data8=np.empty([3, len(data5[:,0])])
    data9=np.empty([3, len(data5[:,0])])
    d1=200
    d2=2
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data[:,0])):
        if (i-d2<=0):
            data4[:, i]=np.average(data2[:, 0:d2], axis=1)
        elif (i+d2>=len(data[:,0])):
            data4[:, i]=np.average(data2[:, -d2:], axis=1)
        else:
            data4[:, i]=np.average(data2[:, i-d2:i+d2+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d1<=0):
            data8[:, i]=np.average(data7[:, 0:d1], axis=1)
        elif (i+d1>=len(data5[:,0])):
            data8[:, i]=np.average(data7[:, -d1:], axis=1)
        else:
            data8[:, i]=np.average(data7[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data5[:,0])):
        if (i-d2<=0):
            data9[:, i]=np.average(data7[:, 0:d2], axis=1)
        elif (i+d2>=len(data5[:,0])):
            data9[:, i]=np.average(data7[:, -d2:], axis=1)
        else:
            data9[:, i]=np.average(data7[:, i-d2:i+d2+1], axis=1)
    f = plt.figure(figsize=(5, 6))
    f1 = f.add_subplot(111)
    f2 = f1.twinx()
    #f3.set_ylim(0,1)
    plt.setp(f2.get_xticklabels(), visible=False)
    #f1.plot(data2[0, :], data4[1, :], color="lightblue")
    #f1.plot(data2[0, :-20], data3[1, :-20], color="darkblue")
    #f1.plot(data7[0, :-20], data8[1, :-20], color="darkviolet")
    #f1.set_ylim(0, data[-1, 2])
    #f2.plot(data2[0, :], data4[2, :], color="lightcoral")
    f1.plot(data2[0, :-20], data3[2, :-20], color="darkred")
    f2.plot(data2[0, :-20], data3[1, :-20], color="lightcoral")
    f1.plot(data7[0, :-20], data8[2, :-20], color="green")
    f2.plot(data7[0, :-20], data8[1, :-20], color="lightgreen")
    xmax=500
    plt.xlim(0, xmax)
    f1.tick_params(labelsize=16)
    f2.tick_params(labelsize=16)
    f2.set_ylabel(r"$\mu_f$", fontsize=18)
    f1.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$f_{\parallel}$(eV/$\AA$)", fontsize=18)
    f1.set_ylim([0,100])
    f1.set_xlim([0, 400])
    f2.set_ylim(0,2)
    plt.tight_layout()
    plt.savefig(image_name)

def post(atom_info):
    bottom=fun.create_bottom(np.array([
            [atom_info[:,:,2].min(), atom_info[:, :, 2].max()],
            [atom_info[:,:,3].min(), atom_info[:, :, 3].max()],
            [atom_info[:,:,4].min(), atom_info[:, :, 4].min()+2]]),
            atom_info)
    fr_coeff, fx, fy, fz = fun.get_friction(bottom, atom_info)
    fun.save_friction_coeff(
        f"HG32/HG32-HG32_5_2_1.data",
        f"temps/HG32-HG32_0x-2.5x5.temp",
        timestep,
        fr_coeff,
        fy,
        fz,
    )
    fun.plot_friction_coeff(
        f"HG32/HG32-HG32_6.out",
        f"HG32/HG32-HG32_5_2_1.data",
        f"HG32-HG32_5.png",
        f"forces.png",
        "2.6.png"
    )

if __name__=="__main__":
    #timestep, n_atoms, n_frames, box_info, atom_info, r, r0 = fun.read_dump(
    #    "dumps/HG32-HG32_0x-2.5x5.dump", ":"
    #)
    #post(atom_info)
    #fun.view_distr(box_info, atom_info, d_freeze=2, disp=[0,0,2.34], path1="ditr.png")
    #forces_z, forces_y, friction_norm, friction_mean=reorder(np.array([forces_z, forces_y, friction_norm, friction_mean]))
    #SiCSiC(forces_z, forces_y, friction_norm, friction_mean)
    #energy("temps/Fe-SiC_ondo.temp", "temps/Fe-SiC_gaizki.temp", "Energy.png", 0.001)
    #RDF_dif("FeNi.eq.dump", "FeNi_npt.eq.dump", "FeNi_analytic.eq.dump", "FeNi_analyticalnpt.eq.dump","FeNi300.cfg", "RDF_dif.png")
    #vol_energy1("Fe.eq.3000.out", "Fe.eq.out", "Fe_2.eq.20.out", "Fe.vol.png", "Fe.eq.3000.out", "Fe.eq.out", "Fe_2.eq.20.out", 0.001, 0.001)
    #vol_energy1("SiC.eq.3000.out", "SiC.eq.out", "SiC_2.eq.20.out", "SiC.vol.png", "SiC.eq.3000.out", "SiC.eq.out", "SiC_2.eq.20.out", 0.001, 0.001)
    fun.plot_friction_coeff(f"HG32/HG32-HG32_6.out",f"HG32/HG32-HG32_3_2.data",f"HG32-HG32_3_2.png",f"forces.png","2.6.png")
    #fun.plot_friction_coeff(f"friction.out",f"SiC-SiC/data/SiC-SiC_2.3.data",f"f_coeff.png",f"forces.png","2.3.png")
    #fun.plot_friction_coeff(f"friction.out",f"SiC-SiC/data/SiC-SiC.data",f"f_coeff.png",f"forces.png", "juntu.png")
    #force_dist_1("SiC-SiC/data/SiC-SiC_2.3.data", "SiC-SiC/data/SiC-SiC_2.6.data","SiC-SiC/data/juntu.data", "konp_1.png")
    #definitive_vel("SiC-SiC/data/SiC-SiC.data", "SiC-SiC_300slow/SiC-SiC.data", "SiC-SiC_300ss/SiC-SiC.data", "SiC-SiC_1000/SiC-SiC.data", "SiC-SiC_1000slow/SiC-SiC.data","SiC-SiC_1000ss/SiC-SiC.data",  "def_vel.png")
    #definitive("SiC-SiC_300ss/SiC-SiC.data", "SiC-SiC_1000ss/SiC-SiC.data", "def_vel.png")
    #definitive_temp("SiC-SiC/data/SiC-SiC.data", "SiC-SiC_1000/SiC-SiC.data", "SiC-SiC_300slow/SiC-SiC.data", "SiC-SiC_1000slow/SiC-SiC.data", "SiC-SiC_300ss/SiC-SiC.data", "SiC-SiC_1000ss/SiC-SiC.data", "def_temp.png")
    #fun.plot_friction_coeff(f"friction.out",f"SiC-SiC/data_1/SiC-SiC_2.3.data",f"f_coeff.png",f"forces.png", "juntu.png")
