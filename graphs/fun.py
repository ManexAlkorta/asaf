import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.animation as animation
from matplotlib.colors import ListedColormap
from matplotlib.patches import Rectangle
import mpl_toolkits.mplot3d.axes3d as p3
import scipy.interpolate as interp1d
import subprocess as sp
import math
from PIL import Image
import time
import os

# from pathlib import Path

k_B = 1.38 * 10 ** (-23)
N_A = 6.022 * 10 ** (23)
# mypath = Path(__file__).parent
path = "Fe-Fe_0x0x5.dump"
path1 = "Cu-SiC_200.dump"
path2 = "saia.dump"
path3 = "Fe.eq.dump"
path4 = "Fe-SiC_-10x0x4.dump"
path5 = "FeNi.cfg"
path6 = "FeNi06_200.dump"
path7 = "FeNi_1_0.1_0.001.dump"
temp_file = "Fe-SiC_-10x0x4.temp"
dt = 0.001  # picoseconds


class group:
    """
    This class contains the information of a given group of atoms.
    """

    def __init__(self, box, atom_info, frame):
        self.x_lo = box[0, 0]
        self.x_hi = box[0, 1]
        self.y_lo = box[1, 0]
        self.y_hi = box[1, 1]
        self.z_lo = box[2, 0]
        self.z_hi = box[2, 1]
        self.box = box
        self.ID = get_ID(box, atom_info, frame)
        if self.ID == []:
            self.status = "Empty"
        else:
            self.status = "Filled"


def get_mass(atom_info: np.ndarray, mass_list: np.ndarray) -> np.ndarray:
    """This function calculates the mass of the group of atoms.

    Args:
        atom_info: The array that contains the atom information. numpy array.
        mass_list: The list of each mass of each atom type in the simulation.
    Returns:
        mass: The mass of each atom. numpy array.
    """
    masses = np.empty(len(atom_info[0, :, 0]))
    for ID in range(len(masses)):
        masses[ID] = mass_list[int(atom_info[0, ID, 1]) - 1]
    return masses


def read_cfg(path: str, n_frames: int):
    file = open(path, "r")
    print("Reading the file...")
    timestep = 1
    for index, line in enumerate(file):
        if index == 2:
            n_atoms = int(line)
    last_index = index
    n = n_atoms + 16
    if n_frames == ":":
        n_frames = int((last_index + 1) / n)
    timestep = np.empty(n_frames)
    box_info = np.empty([n_frames, 3, 3])
    atom_info = np.empty([n_frames, n_atoms, 8], dtype="U20")
    r = np.empty([n_frames, n_atoms, 3])
    data = np.empty(n, dtype="U200")
    index = 0
    file.seek(0)
    while index < n_frames:
        i = 0
        while i < n:
            data[i] = (file.readline())[:-1]
            i += 1
        timestep[index] = dt * index
        for j in range(0, 3):
            box_info[index, j] = np.array(data[4 + j].split(), dtype="U20")
        for k in range(0, n_atoms):
            atom_info[index, k] = np.array(data[8 + k].split(), dtype="U20")
        r[index] = atom_info[index, :, 2:5]
        index += 1
    r0 = r[0, :]
    print("File read: {} frames, {} atoms.".format(n_frames, n_atoms))
    return (timestep, n_atoms, n_frames, box_info, atom_info, r, r0)


def read_dump(path: str, n_frames: int):
    """This function reads the information from .dump files.

    Args:
        path: The directory of the .dump file.
        n_frames: The number of frames to read.

    Returns:
        timestep: The time of the simulation for each frame. numpuy.array.
        n_atoms: The number of atoms used in the simulation. int.
        n_frames: The number of frames read by the function. int.
        box_info: The dimensions of the supercell of the simulation.
            numpy.array.
        atom_info: The position, velocity and forces vectors for each atom.
            numpy.array.
        r: The position of each atom. np.array.
        r0: The position of each atom in the first frame. numpy.array.
    """
    file = open(path, "r")
    print("Reading the file...")
    for index, line in enumerate(file):
        if index == 3:
            n_atoms = int(line)
    file = open(path, "r")
    last_index = index
    n = n_atoms + 9
    if n_frames == ":":
        n_frames = int((last_index + 1) / n)
    timestep = np.empty(n_frames)
    box_info = np.empty([n_frames, 3, 2])
    atom_info = np.empty([n_frames, n_atoms, 11])
    r = np.empty([n_frames, n_atoms, 3])
    data = np.empty(n, dtype="U200")
    index = 0
    while index < n_frames:
        i = 0
        while i < n:
            data[i] = (file.readline())[:-1]
            i += 1
        timestep[index] = dt * (data[1]).astype(float)
        box_info_list = np.char.split(data[5:8], sep=" ", maxsplit=2)
        box_info[index] = array_of_lists_to_array(box_info_list)
        atom_info_list = np.char.split(data[9:], sep=" ", maxsplit=10)
        atom_info[index] = array_of_lists_to_array(atom_info_list)
        r[index] = atom_info[index, :, 2:5]
        index += 1
    r0 = r[0, :]
    print("File read: {} frames, {} atoms.".format(n_frames, n_atoms))
    return (timestep, n_atoms, n_frames, box_info, atom_info, r, r0)


def get_RMSD(
    n_atoms: int,
    n_frames: int,
    atom_info: np.ndarray,
    r0: np.ndarray,
    r: np.ndarray,
) -> np.ndarray:
    print("Calculating the RMSD...")
    start = time.time()
    RMSD = np.zeros(n_frames, dtype=float)
    for i in range(n_frames):
        for j in range(n_atoms):
            RMSD[i] += np.sum(np.power(r0[j] - r[i, j], 2))
    RMSD = RMSD / n_atoms
    RMSD = np.sqrt(RMSD)
    end = time.time()
    print(end - start)
    return RMSD


def get_RDF(
    n_atoms: int,
    n_frames: int,
    frame: int,
    atom_info: np.ndarray,
    r: np.ndarray,
    R_cut: float,
    n: float,
) -> np.ndarray:
    RDF = np.zeros([n])
    h = R_cut / n
    for atom in range(0, n_atoms):
        for atom1 in range(0, n_atoms):
            R = np.sqrt(np.sum((r[frame, atom] - r[frame, atom1]) ** 2))
            if R < R_cut:
                for i in range(0, n):
                    if h * i < R < h * (i + 1):
                        RDF[i] += 1
    return RDF / n_atoms


def RDF(
    n_atoms: int,
    n_frames: int,
    frame: int,
    atom_info: np.ndarray,
    r: np.ndarray,
    R_cut: float,
    n: float,
    path: str,
) -> np.ndarray:
    x = np.arange(0, R_cut, R_cut / n)
    RDF = get_RDF(n_atoms, n_frames, frame, atom_info, r, R_cut, n)
    plt.plot(x, RDF)
    plt.savefig(path)


def R_atoms(
    n_atoms: int,
    n_frames: int,
    frame: int,
    atom_info: np.ndarray,
    r: np.ndarray,
    R_cut: float,
    n: float,
) -> np.ndarray:
    atoms = 0
    for atom in range(0, n_atoms):
        for atom1 in range(0, n_atoms):
            R = np.sqrt(np.sum((r[frame, atom] - r[frame, atom1]) ** 2))
            if R < R_cut:
                atoms += 1
    return atoms / n_atoms


def save_RMSD(path: str, timestep: np.ndarray, RMSD: np.ndarray):
    """This function saves the RMSD.

    Args:
        -path: The name of the .data file.
        -timestep: The simulation time. np.ndarray.
        -RMSD: The RMSD for each timestep. np.ndarray.
    Return:
        -None.
    """
    data = np.empty([len(timestep), 2])
    data[:, 0] = timestep
    data[:, 1] = RMSD
    np.savetxt(path, data, delimiter="\t", newline="\n")


def RMSD(
    path: str,
    path1: str,
    n_atoms: int,
    n_frames: int,
    atom_info: np.ndarray,
    r0: np.ndarray,
    r: np.ndarray,
):
    """This function calculates, saves and plots the RMSD.

    Args:
        - path: The name of the file to save the data.
        - path1: The name of the image of the plot.
        - n_atoms: The number of atoms in the system.
        - n_frames: The number of frames of the system.
        - atom_info: The information of each atom.
        - r0: The initial state of each atom.
        - r: The position of each atom in time."""
    RMSD = get_RMSD(n_atoms, n_frames, atom_info, r0, r)
    save_RMSD(path, timestep, RMSD)
    plot_RMSD(path, path1)


def array_of_lists_to_array(arr):
    return np.apply_along_axis(
        lambda a: np.array(a[0], dtype=float), -1, arr[..., None]
    )


def get_ID(box: np.ndarray, atom_info: np.ndarray, frame: int) -> list:
    """This function returns the ID of the atoms in a given region.

    Args:
        - box: The region in which to get the IDs.
        - atom_info: The atomic information of each atom.
        - frame: The frame in which to calculate the IDs.
    Returns:
        - groupID: The list of all the IDs.
    """
    groupID = []
    for i in range(len(atom_info[0, :, 0])):
        if (
            box[0, 0] < atom_info[frame, i, 2] < box[0, 1]
            and box[1, 0] < atom_info[frame, i, 3] < box[1, 1]
            and box[2, 0] < atom_info[frame, i, 4] < box[2, 1]
        ):
            groupID.append(i)
    return groupID


def create_surfaces(atom_info, s_depth, disp):
    box1, box2 = box0(atom_info, s_depth, disp)
    s_botBOX = np.array(
        [
            [(box1[0,1]-box1[0, 0])/2-s_depth[0], (box1[0,1]-box1[0, 0])/2+s_depth[0]],
            [(box1[1,1]-box1[1, 0])/2-s_depth[1], (box1[1,1]-box1[1, 0])/2+s_depth[1]],
            [box1[2, 1] - s_depth[2], box1[2, 1]],
        ]
    )
    s_topBOX = np.array(
        [
            [(box2[0,1]-box2[0, 0])/2-s_depth[0], (box2[0,1]-box2[0, 0])/2+s_depth[0]],
            [(box2[1,1]-box2[1, 0])/2-s_depth[1], (box2[1,1]-box2[1, 0])/2+s_depth[1]],
            [box2[2, 0], box2[2, 0] + s_depth[2]],
        ]
    )
    s_bot = group(s_botBOX, atom_info, 0)
    s_top = group(s_topBOX, atom_info, 0)
    return (s_bot, s_top)


def create_layer(atom_info, disp, d_freeze):
    box1, box2 = box0(atom_info[0, :, 2:5], disp)
    layerBOX = np.array(
        [
            [box2[0, 0], box2[0, 1]],
            [box2[1, 0], box2[1, 1]],
            [box2[2, 1] - d_freeze, box2[2, 1] - d_freeze + 2],
        ]
    )
    layer = group(layerBOX, atom_info)
    return layer


def box0(atom_info, s_depth, disp):
    x1_lo, x1_hi, y1_lo, y1_hi = 0, 0, 0, 0
    x2_lo, x2_hi, y2_lo, y2_hi = 0, 0, 0, 0
    for ID in range(len(atom_info[0, :, 0])):
        if atom_info[0,ID,5]<0:
            if atom_info[0,ID,2]<x1_lo:
                x1_lo=atom_info[0,ID,2]
            if atom_info[0,ID,2]>x1_hi:
                x1_hi=atom_info[0,ID,2]
            if atom_info[0,ID,3]<y1_lo:
                y1_lo=atom_info[0,ID,3]
            if atom_info[0,ID,3]>y1_hi:
                y1_hi=atom_info[0,ID,3]
        if atom_info[0,ID,5]>0:
            if atom_info[0,ID,2]<x2_lo:
                x2_lo=atom_info[0,ID,2]
            if atom_info[0,ID,2]>x2_hi:
                x2_hi=atom_info[0,ID,2]
            if atom_info[0,ID,3]<y2_lo:
                y2_lo=atom_info[0,ID,3]
            if atom_info[0,ID,3]>y2_hi:
                y2_hi=atom_info[0,ID,3]
    z1_lo = atom_info[0, :, 2:5][:, 2].min()
    z1_hi = 0
    z2_lo = disp[2]
    z2_hi = atom_info[0, :, 2:5][:, 2].max()
    box1 = np.array([[x1_lo, x1_hi], [y1_lo, y1_hi], [z1_lo, z1_hi]])
    box2 = np.array([[x2_lo, x2_hi], [y2_lo, y2_hi], [z2_lo, z2_hi]])
    return (box1, box2)


def get_friction(group, atom_info):
    fr_coeff = np.empty([len(atom_info[:, 0, 0])], dtype=float)
    fx, fy, fz = np.zeros(len(atom_info[:,0,0])), np.zeros(len(atom_info[:,0,0])), np.zeros(len(atom_info[:,0,0]))
    for ID in group.ID:
        fx += atom_info[:, ID, 8]
        fy += atom_info[:, ID, 9]
        fz += atom_info[:, ID, 10]
    for i in range(0, len(atom_info[:, 0, 0])):
        try:
            fr_coeff[i] = abs(fy[i] / fz[i])
        except RuntimeWarning:
            fr_coeff[i] = 0
    return (fr_coeff, fx, fy, fz)

def create_bottom(box, atom_info):
    bottom = group(box, atom_info, 1)
    return bottom


def get_temp_from_vel(group, atom_info, mass, frame):
    """This function calculates the temp of a group by from their velocities.

    Args:
        group: A group of atoms. object.
        atom_info: The info of each atom in each frame. numpy array.
    Return:
        temp: The temperature of the group in each frame. numpy array.
    """
    vx = vy = vz = np.zeros(len(group.ID))
    # vel units: Angstrom/picosecond. vel ** 2 --> x10⁴ --> m/s
    # mass units: atomic mass. mass --> x1/N_A/10**3 --> grams
    i = 0
    KE = np.empty(len(group.ID))
    if group.status == "Empty":
        temp = 1
    else:
        # Evaluate if the group of atoms is filled. If it's empty return T=0K.
        for ID in group.ID:
            vx[i] = atom_info[frame, ID, 5]
            vy[i] = atom_info[frame, ID, 6]
            vz[i] = atom_info[frame, ID, 7]
            i += 1
        vx_avg = np.average(vx)
        vy_avg = np.average(vy)
        vz_avg = np.average(vz)
        vx -= vx_avg
        vy -= vy_avg
        vz -= vz_avg
        # The average velocity of the system must be null to evaluate T.
        KE = mass[ID] * np.sum(np.power(np.array([vx, vy, vz]), 2), axis=0)
        temp = np.average(KE) * 10 / (3 * k_B * N_A)
        if temp < 1:
            temp = 1
    # Units: Kelvin.
    return temp


def save_friction_coeff(path, temp_file, timestep, fr_coeff, f_lat, fz):
    names, parameters = read_parameters(temp_file)
    data = np.empty([len(timestep), 7])
    fr_coeff_ave = np.empty(len(fr_coeff))
    norm = 0
    for i in range(len(fr_coeff)):
        fr_coeff_ave[i] = np.average(fr_coeff[: i + 1])
    data[:, 0] = timestep
    data[:, 1] = fr_coeff
    data[:, 2] = fr_coeff_ave
    data[:, 3] = f_lat
    data[:, 4] = fz
    data[:, 5] = parameters[:, 0]
    for i in range (0, len(timestep)):
        if data[i,1]<data[-1,2]:
            data[i,6]=data[i,1]
            norm+=1
        else:
            data[i, 6] = data[i,1]*math.e**(-0.5*((data[i,1]-data[-1, 2]))**2)
            norm+=math.e**(-0.5*((data[i,1]-data[-1,2]))**2)
    data[:,6]=len(data[:,6])/norm*data[:,6]
    np.savetxt(path, data, delimiter="\t", newline="\n")


def plot_RMSD(path, path1):
    data = np.loadtxt(path, delimiter="\t")
    xmax = data[-1, 0]
    f = plt.figure(figsize=(10, 6))
    f1 = f.add_subplot(1, 1, 1)
    f1.set_ylabel(r"RMSD ($\AA$)")
    f1.set_xlabel(r"t ($ps$)")
    f1.set_xlim(0, xmax)
    plt.plot(data[:, 0], data[:, 1], color="tab:blue", label="RMSD")
    f1.legend()
    plt.savefig(path1)

def get_force_distr(xmax, dx, data):
    x=np.arange(0, xmax, dx)
    dist=np.zeros(len(x))
    i=0
    for fz in data[0, :]:
        if fz>0:
            while (i<len(x)-1):
                try:
                    if x[i-1]<fz<x[i+2]:
                        dist[i]+=1
                        break
                    else:
                        i+=1
                except IndexError:
                    if x[i]<fz<x[i+1]:
                        dist[i]+=1
                        break
                    else:
                        i+=1
    return (dist, x)

def plot_friction_coeff(file, path, path1, path2, path3):
    data = np.loadtxt(path, delimiter="\t")
    data1=np.empty([len(data[:,0]), len(data[0,:])])
    data[:, 4]=-data[:, 4]
    d=50
    for i in range(0, len(data[:,0])):
        if (i-d<=0):
            data1[i, :]=np.average(data[0:d, :], axis=0)
        elif (i+d>=len(data[:,0])):
            data1[i, :]=np.average(data[-d:, :], axis=0)
        else:
            data1[i, :]=np.average(data[i-d:i+d+1, :], axis=0)
    ke, diff, i = data[-1, 5], 0, 1
    while abs(diff) < 0.05 * ke:
        try:
            diff = data[-1, 5] - data[-i, 5]
            i += 1
        except IndexError:
            i=len(data[:,5])-1
    # The calculation starts when the potential energy is stablished.
    file = open(file, "a")
    print(
        f"Friction coeff (normalized): {np.average(data[:, 6])}",
        file=file,
    )
    print(
        f"Friction coeff (mean): {np.average(data[:, 3]/np.average(data[-i:, 4]))}",
        file=file,
    )
    print(f"fy: {np.average(data[-i:, 3])}", file=file)
    print(f"fz: {np.average(data[-i:, 4])}", file=file)
    file.close()
    xmax = data[-1, 0]
    f = plt.figure(figsize=(8, 6))
    f1 = f.add_subplot(211)
    f1.set_ylabel(r"$\mu_F$", fontsize=18)
    f1.set_xlabel(r"$t$ (ps)", fontsize=18)
    f1.tick_params(labelsize=16)
    plt.setp(f1.get_xticklabels(), visible=False)
    f1.set_xlim(0, xmax)
    #f1.plot(data[:, 0], data[:, 1], color="lightblue", label=r"$\mu_F$")
    f1.plot(data[:, 0], data1[:, 3]/data1[:,4], color="darkblue", label=r"$\mu_F$")
    f1.set_ylim(-0.1,0.1)
    yticks = f1.yaxis.get_major_ticks()
    yticks[0].label1.set_visible(False)
    f2 = f.add_subplot(212, sharex=f1)
    plt.subplots_adjust(hspace=.0, wspace=.0)
    f2.set_ylabel(r"$F\,(eV/{\AA})$", fontsize=18)
    f2.set_xlabel(r"$t$ (ps)", fontsize=18)
    f2.plot(data[:, 0], data1[:, 3], color="tab:orange", label=r"$F_y$")
    f2.plot(data[:, 0], data1[:, 4], color="tab:red", alpha=0.8, label=r"$F_z$")
    f2.tick_params(labelsize=16)
    plt.savefig(path1)
    data2=reorder(np.array([data1[:, 4], data1[:, 1], data1[:, 3]]))
    data3=np.empty([3, len(data[:,0])])
    data4=np.empty([3, len(data[:,0])])
    d1=300
    d2=2
    for i in range(0, len(data[:,0])):
        if (i-d1<=0):
            data3[:, i]=np.average(data2[:, 0:d1], axis=1)
        elif (i+d1>=len(data[:,0])):
            data3[:, i]=np.average(data2[:, -d1:], axis=1)
        else:
            data3[:, i]=np.average(data2[:, i-d1:i+d1+1], axis=1)
    for i in range(0, len(data[:,0])):
        if (i-d2<=0):
            data4[:, i]=np.average(data2[:, 0:d2], axis=1)
        elif (i+d2>=len(data[:,0])):
            data4[:, i]=np.average(data2[:, -d2:], axis=1)
        else:
            data4[:, i]=np.average(data2[:, i-d2:i+d2+1], axis=1)
    f = plt.figure(figsize=(10, 6))
    f1 = f.add_subplot(111)
    f2 = f1.twinx()
    f3 = f1.twinx()
    #f3.set_ylim(0,1)
    plt.setp(f3.get_xticklabels(), visible=False)
    plt.setp(f3.get_yticklabels(), visible=False)
    #f1.plot(data2[0, :], data4[1, :], color="lightblue")
    f1.plot(data2[0, :-20], data3[1, :-20], color="darkblue")
    f1.set_ylim(0, data[-1, 2])
    #f2.plot(data2[0, :], data4[2, :], color="lightcoral")
    f2.plot(data2[0, :-20], data3[2, :-20], color="darkred")
    xmax=500
    plt.xlim(0, xmax)
    f1.tick_params(labelsize=16)
    f1.set_ylabel(r"$\mu_f$", fontsize=18)
    f1.set_xlabel(r"$f_{\perp}$(eV/$\AA$)", fontsize=18)
    f2.tick_params(labelsize=16)
    f2.set_ylabel(r"$f_{\parallel}$(eV/$\AA$)", fontsize=18)
    #dist, x=get_force_distr(400, 10, data3)
    xnew=np.arange(0, xmax, 1)
    dist, x, patches=plt.hist(data[:, 4], bins=10, alpha=0)
    try:
        distnew=interp1d.interp1d(x[:-1], dist, kind='cubic')
        f3.fill_between(xnew, distnew(xnew), step="pre", alpha=0.2, color="orange")
        f3.plot(xnew, distnew(xnew), color="orange")
    except ValueError:
        x1=np.arange(x[-1]+1, 500, 1)
        dist1=np.zeros([int(500-x[-1])])
        distnew=interp1d.interp1d(np.append(x[:-1],x1), np.append(dist,dist1), kind='cubic')
        f3.fill_between(xnew, distnew(xnew), step="pre", alpha=0.2, color="orange")
        f3.plot(xnew, distnew(xnew), color="orange")
    f3.set_yticks([])
    #f3.set_ylim(0,150)
    plt.tight_layout()
    plt.savefig(path3)
    



def plot_parameters(path, path1, timestep):
    a = False
    data = np.loadtxt(path, delimiter=" ", dtype=str)
    names = data[0, :]
    data = data[1:, :].astype(float)
    xmax = timestep[-1]
    f = plt.figure(figsize=(10, 6))
    f1 = f.add_subplot(1, 1, 1)
    f1.set_xlabel(r"$t$ (ps)")
    f1.set_xlim(0, xmax)
    for i in range(len(names)):
        if names[i][0] == "T" and a is False:
            a = True
            f = plt.figure(figsize=(10, 6))
            f1 = f.add_subplot(1, 1, 1)
            f1.set_xlabel(r"$t$ (ps)")
            f1.set_xlim(0, xmax)
        plt.plot(timestep[:], data[:, i], label=f"{names[i]}")
    f1.legend()
    plt.savefig(path1)


def view_distr(box_info, atom_info, d_freeze, disp, path1):
    z_len = atom_info[0, :, 4].max()-atom_info[0, :, 4].min()
    d=0.1
    a = int(z_len // d + 1)  # How many groups
    z = np.arange(atom_info[0, :, 4].min(), atom_info[0, :, 4].min() + a * d, d)
    forces = np.empty([len(atom_info[:, 0, 0]), a, 3])
    for i in range(a):
        g1 = group(
            np.array(
                [
                    [atom_info[0, :, 2].min(), atom_info[0, :, 2].max()],
                    [atom_info[0, :, 3].min(), atom_info[0, :, 3].max()],
                    [
                        atom_info[0, :, 4].min() + i * d,
                        atom_info[0, :, 4].min() + (i + 1) * d,
                    ],
                ]
            ),
            atom_info,
            0,
        )
        try:
            fr_coeff, fx, fy, fz = get_friction(g1, atom_info)
        except ZeroDivisionError:
            fx, fy, fz = (
                np.zeros(len(atom_info[:, 0, 0])),
                np.zeros(len(atom_info[:, 0, 0])),
                np.zeros(len(atom_info[:, 0, 0])),
            )
        forces[:, i, 0], forces[:, i, 1], forces[:, i, 2] = fx, fy, fz
    f = plt.figure(figsize=(5, 6))
    f1 = f.add_subplot(111)
    f2 = f1.twiny()
    f2.set_xlim([atom_info[0, :, 2].min()-
        (atom_info[0, :, 2].max()-atom_info[0,:,2].min())*0.1,
        atom_info[0, :, 2].max()+
        (atom_info[0, :, 2].max()-atom_info[0,:,2].min())*0.1])
    f1.set_xlabel(r"$F\,(eV/\mathrm{\AA}$)", fontsize=18)
    f1.set_ylabel(r"$z(\mathrm{\AA}$)", fontsize=18)
    f2.set_xticks([])
    f1.tick_params(labelsize=16)
    f2.tick_params(labelsize=16)
    f2.add_patch(Rectangle((atom_info[0, :, 2].min(), atom_info[0, :, 4].min()),
                    atom_info[0, :, 2].max()-atom_info[0, :, 2].min(),
                    -atom_info[0, :, 4].min(),linewidth=0, color="lightblue",alpha=0.1))
    f2.add_patch(Rectangle((atom_info[0, :, 2].min()+d_freeze, atom_info[0, :, 4].min()),
                    atom_info[0, :, 2].max()-atom_info[0, :, 2].min()-2*d_freeze,
                    d_freeze, color="darkblue",linewidth=0,alpha=0.2))
    f2.add_patch(Rectangle((atom_info[0, :, 2].min(), atom_info[0, :, 4].min()),
                    d_freeze,
                    -atom_info[0, :, 4].min(), color="darkblue",linewidth=0,alpha=0.2))
    f2.add_patch(Rectangle((atom_info[0, :, 2].max()-d_freeze, atom_info[0, :, 4].min()),
                    d_freeze,
                    -atom_info[0, :, 4].min(), color="darkblue",linewidth=0,alpha=0.2))
    f2.add_patch(Rectangle((atom_info[0, :, 2].min(), disp[2]),
                    atom_info[0, :, 2].max()-atom_info[0, :, 2].min(),
                    -atom_info[0, :, 4].min(), color="lightblue", linewidth=0, alpha=0.1))
    f2.add_patch(Rectangle((atom_info[0, :, 2].min()+d_freeze, atom_info[0, :, 4].max()-d_freeze),
                    atom_info[0, :, 2].max()-atom_info[0, :, 2].min()-2*d_freeze,
                    d_freeze, color="darkblue",linewidth=0,alpha=0.2))
    f2.add_patch(Rectangle((atom_info[0, :, 2].min(), disp[2]),
                    d_freeze,
                    -atom_info[0, :, 4].min(), color="darkblue",linewidth=0,alpha=0.2))
    f2.add_patch(Rectangle((atom_info[0, :, 2].max()-d_freeze, disp[2]),
                    d_freeze,
                    -atom_info[0, :, 4].min(), color="darkblue",linewidth=0,alpha=0.2))
    z1=np.arange(z.min(),z.max(),0.1)
    # f_y=interp1d.interp1d(z, np.average(forces[:,:,1], axis=0), kind="cubic")
    # f_z=interp1d.interp1d(z, np.average(forces[:,:,2], axis=0), kind="cubic")
    # f1.plot(f_y(z1), z1, color="orange", label=r"$F_y$", zorder=100)
    # f1.plot(f_z(z1), z1, color="darkred", label=r"$F_z$", zorder=100)
    plt.tight_layout()
    plt.savefig(path1)


def read_parameters(path):
    data = np.loadtxt(path, delimiter=" ", dtype=str)
    names = data[0, :]
    data = data[1:, :].astype(float)
    return (names, data)


def plot_temp_surface(
    folder_name, video_name, g, box_info, atom_info, mass, n: int
):
    """This functions plots the temperature in the surface.

    It creates a movie of the temp over time.

    Args:
        g: The big group of atom to evaluate the temp.
        box_size: The size of
    Returns:
        None
    """
    sp.run(f"mkdir {folder_name}", shell=True)
    dx = box_info[0] / n
    dy = box_info[1] / n
    grid_x, grid_y = np.mgrid[0:10:100j, 0:10:100j]
    points = np.empty([n ** 2, 2])
    values = np.empty([len(atom_info[:, 0, 0]), n ** 2])
    for frame in range(len(atom_info[:, 0, 0])):
        index = 0
        for i in range(4):
            for j in range(4):
                points[index, 0] = dx * i + dx / 2
                points[index, 1] = dy * j + dy / 2
                g1 = group(
                    np.array(
                        [
                            [dx * i - dx / 2, dx * i + dx / 2],
                            [dy * j - dy / 2, dy * j + dy / 2],
                            [g.z_lo, g.z_hi],
                        ],
                    ),
                    atom_info,
                )
                values[frame, index] = get_temp_from_vel(
                    g1, atom_info, mass, frame
                )
                index += 1
    max = values.max()
    for frame in range(len(atom_info[:, 0, 0])):
        grid_temp = interp1d.griddata(
            points,
            values[frame],
            (grid_x, grid_y),
            method="cubic",
        )
        f = plt.figure(frameon=False)
        ax = plt.Axes(f, [0.0, 0.0, 1.0, 1.0])
        ax.set_axis_off()
        f.add_axes(ax)
        plt.imshow(
            grid_temp.T, extent=(0, 1, 0, 1), origin="lower", vmin=0, vmax=max
        )
        plt.title("Cubic")
        f.savefig(f"{folder_name}/{frame}.png")
        plt.close(f)
    create_video(folder_name, video_name)

def reorder(matrix):
    mat1=np.zeros(np.shape(matrix))
    for i in range(0, len(matrix[0,:])):
        for j in range(0, len(matrix[0, :])):
            if matrix[0,i]<=mat1[0, j] or 0==mat1[0, j]:
                for k in range(len(mat1[0,:])-1, j, -1):
                    mat1[:, k]=mat1[:, k-1]
                mat1[:, j]=matrix[:, i]
                break
    return(mat1)

def visualize(
    folder_name, video_name, g, box_info, atom_info, mass, n: int, mode
):
    """ """
    print(f'Visualizing in mode "{mode}"')
    # modes = ["ID", "Temp"]
    sp.run(f"mkdir {folder_name}", shell=True)
    colors = {0: "green", 1: "blue", 2: "red", 3: "black"}
    if mode == "Temp":
        # cmap = plt.cm.get_cmap("viridis")
        cmap = create_colormap(np.array([[0, 255], [0, 0], [255, 0]]))
        values = np.empty([len(atom_info[:, 0, 0]), n ** 3])
        x = np.linspace(box_info[0, 0, 0], box_info[0, 0, 1], n)
        y = np.linspace(box_info[0, 1, 0], box_info[0, 1, 1], n)
        z = np.linspace(box_info[0, 2, 0], box_info[0, 2, 1], n)
        dx = dy = dz = 5
        points = (x, y, z)
        X, Y, Z = np.meshgrid(x, y, z)
        for frame in range(len(atom_info[:, 0, 0])):
            n_point = 0
            for i in range(n):
                for j in range(n):
                    for k in range(n):
                        g1 = group(
                            np.array(
                                [
                                    [X[i, j, k] - dx / 2, X[i, j, k] + dx / 2],
                                    [Y[i, j, k] - dy / 2, Y[i, j, k] + dy / 2],
                                    [Z[i, j, k] - dz / 2, Z[i, j, k] + dz / 2],
                                ]
                            ),
                            atom_info,
                            frame,
                        )
                        values[frame, n_point] = get_temp_from_vel(
                            g1, atom_info, mass, frame
                        )
                        n_point += 1
    values_plotting = normalize(np.log(values), 1)
    zifrak = len(str(len(atom_info[:, 0, 0])))
    for frame in range(len(atom_info[:, 0, 0])):
        color_values = np.reshape(values_plotting[frame], (5, 5, 5))
        f = plt.figure(figsize=[6.4, 6.4])
        plt.subplots_adjust(left=0, right=1, bottom=0, top=1)
        ax = plt.axes(projection="3d")
        ax.view_init(elev=0, azim=-90)
        for ID in range(len(atom_info[0, :, 0])):
            if mode == "ID":
                ax.scatter(
                    atom_info[frame, ID, 2],
                    atom_info[frame, ID, 3],
                    atom_info[frame, ID, 4],
                    color=colors[atom_info[0, ID, 1]],
                    linewidths=5,
                )
            elif mode == "Temp":
                try:
                    ax.scatter(
                        atom_info[frame, ID, 2],
                        atom_info[frame, ID, 3],
                        atom_info[frame, ID, 4],
                        color=cmap(
                            interp1d.interpn(
                                points,
                                color_values,
                                np.array(
                                    [
                                        atom_info[frame, ID, 2],
                                        atom_info[frame, ID, 3],
                                        atom_info[frame, ID, 4],
                                    ],
                                ),
                            ),
                        ),
                        linewidths=5,
                    )
                except ValueError:
                    # Some atoms are too far away to interpolate their T.
                    ax.scatter(
                        atom_info[frame, ID, 2],
                        atom_info[frame, ID, 3],
                        atom_info[frame, ID, 4],
                        color=cmap(0),
                        linewidths=5,
                    )
        plt.axis("off")
        ax.set_xlim([box_info[0, 0, 0], box_info[0, 0, 1]])
        ax.set_ylim([box_info[0, 1, 0], box_info[0, 1, 1]])
        ax.set_zlim([box_info[0, 2, 0], box_info[0, 2, 1]])
        while len(str(frame)) != zifrak:
            frame = "0" + str(frame)
        plt.savefig(f"{folder_name}/{frame}")
        plt.close(f)
    create_video(folder_name, video_name)


def visualize1(
    video_name,
    box_info,
    atom_info,
    mass,
    n: int,
    mode,
    angles,
    elev,
):
    """ """
    print(f'Visualizing in mode "{mode}"')
    # modes = ["ID", "Temp"]
    values = np.empty([len(atom_info[:, 0, 0]), n ** 3])
    x = np.linspace(box_info[0, 0, 0], box_info[0, 0, 1], n)
    y = np.linspace(box_info[0, 1, 0], box_info[0, 1, 1], n)
    z = np.linspace(box_info[0, 2, 0], box_info[0, 2, 1], n)
    dx = dy = dz = 5
    points = (x, y, z)
    X, Y, Z = np.meshgrid(x, y, z)
    for frame in range(len(atom_info[:, 0, 0])):
        n_point = 0
        for i in range(n):
            for j in range(n):
                for k in range(n):
                    g1 = group(
                        np.array(
                            [
                                [X[i, j, k] - dx / 2, X[i, j, k] + dx / 2],
                                [Y[i, j, k] - dy / 2, Y[i, j, k] + dy / 2],
                                [Z[i, j, k] - dz / 2, Z[i, j, k] + dz / 2],
                            ]
                        ),
                        atom_info,
                        frame,
                    )
                    values[frame, n_point] = get_temp_from_vel(
                        g1, atom_info, mass, frame
                    )
                    n_point += 1

    f = plt.figure()
    ax = p3.Axes3D(f, auto_add_to_figure=False)
    f.add_axes(ax)
    ani = animation.FuncAnimation(
        f,
        update,
        len(atom_info[:, 0, 0]),
        fargs=(atom_info, box_info, elev, angles, ax, points, values, mode),
    )
    ani.save(f"{video_name}")


def gen_time(atom_info):
    for t in range(len(atom_info)):
        yield (atom_info[t, :, 2:5])


def update(frame, atom_info, box_info, elev, azim, ax, points, values, mode):
    ax.clear()
    azim = frame * 360 / len(atom_info[:, 0, 0])
    plt.axis("off")
    ax.set_xlim([box_info[0, 0, 0], box_info[0, 0, 1]])
    ax.set_ylim([box_info[0, 1, 0], box_info[0, 1, 1]])
    ax.set_zlim([box_info[0, 2, 0], box_info[0, 2, 1]])
    ax.view_init(elev=elev, azim=azim)
    if mode == "Temp":
        values_plotting = normalize(np.log(values), 1)
        color_values = np.reshape(values_plotting[frame], (5, 5, 5))
        cmap = create_colormap(np.array([[0, 255], [0, 0], [255, 0]]))
        for ID in range(len(atom_info[0, :, 0])):
            try:
                ax.scatter(
                    atom_info[frame, ID, 2],
                    atom_info[frame, ID, 3],
                    atom_info[frame, ID, 4],
                    color=cmap(
                        interp1d.interpn(
                            points,
                            color_values,
                            np.array(
                                [
                                    atom_info[frame, ID, 2],
                                    atom_info[frame, ID, 3],
                                    atom_info[frame, ID, 4],
                                ],
                            ),
                        ),
                    ),
                    linewidths=5,
                )
            except ValueError:
                # Some atoms are too far away to interpolate their T.
                ax.scatter(
                    atom_info[frame, ID, 2],
                    atom_info[frame, ID, 3],
                    atom_info[frame, ID, 4],
                    color=cmap(0),
                    linewidths=5,
                )
    elif mode == "ID":
        colors = {0: "green", 1: "blue", 2: "red", 3: "black"}
        for ID in range(len(atom_info[0, :, 0])):
            ax.scatter(
                atom_info[frame, ID, 2],
                atom_info[frame, ID, 3],
                atom_info[frame, ID, 4],
                color=colors[atom_info[0, ID, 1]],
                linewidths=5,
            )


def create_video(path: str, video_name: str):
    """This function creates a movie from images in a given folder.

    Args:
        -path: The folders name. str.
    Returns:
        None
    """
    image_name = os.listdir(path)
    image_name.sort()
    img = []
    for image in image_name:
        img.append(Image.open(path + f"/{image}"))
    f = plt.figure(figsize=[6.4, 6.4])
    plt.subplots_adjust(left=0, right=1, bottom=0, top=1)
    plt.axis("off")
    frames = []
    for i in range(len(img)):
        frames.append([plt.imshow(img[i], cmap=cm.Greys_r, animated=True)])
    ani = animation.ArtistAnimation(
        f, frames, interval=200, repeat=False, blit=True
    )
    ani.save(f"{video_name}")
    plt.show()
    plt.close(f)


def normalize(array: np.ndarray, value: float):
    """This function normalizes an array to a given max value."""
    max = array.max()
    a = value / max
    return array * a


def create_colormap(values) -> ListedColormap:
    """This function creates a custom cmap.

    Args:
        - values: The rgba values.
    Returns:
        - cmap: Colormap."""
    vals = np.ones([256, 4])
    vals[:, 0] = np.linspace(values[0, 0] / 256, values[0, 1] / 256, 256)
    vals[:, 1] = np.linspace(values[1, 0] / 256, values[1, 1] / 256, 256)
    vals[:, 2] = np.linspace(values[2, 0] / 256, values[2, 1] / 256, 256)
    cmap = ListedColormap(vals)
    return cmap


if __name__ == "__main__":
    timestep, n_atoms, n_frames, box_info, atom_info, r, r0 = read_dump(
        "FeNi.eq.dump", ":"
    )
    frame=50
    RDF(n_atoms, n_frames, frame, atom_info, r, R_cut=5, n=100, path="FeNi.eq.png")
